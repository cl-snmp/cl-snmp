(in-package "SNMP")

(defun rfc1514-value (subids)
  (case (pop subids)
    ((1)					      ;hrSystem
     (case (pop subids)
       ((1)					      ;hrSystemUptime
	(and (= (pop subids) 0)
	     (system-uptime)))
       ((2)					      ;hrSystemDate
	(and (= (pop subids) 0)
	     (rfc1514-date-and-time)))
       ((3)					      ;hrSystemInitialLoadDevice
	(and (= (pop subids) 0) 
	     (rfc1514-initial-load-device-index)))
       ((4)					      ;hrSystemInitialLoadParameters
	(and (= (pop subids) 0)
	     (rfc1514-initial-load-parameters)))
       ((5)					      ;hrSystemNumUsers
	(and (= (pop subids) 0)
	     (rfc1514-num-users)))
       ((6)					      ;hrSystemProcesses
	(and (= (pop subids) 0)
	     (length sys:all-processes)))
       ((7))))					      ;hrSystemMaxProcesses

    ((2)					      ;hrStorage
     (case (pop subids)				      ;hrStorageRamDisc
       ((2)					      ;hrMemorySize
	(let ((pages 0))
	  (dotimes (index sys:main-memory-map-size)
	    (incf pages (storage::main-memory-map-n-pages index)))
	  (truncate (storage::wordskilobits (* pages sys:page-size)) 8)))
       ((3)					      ;hrStorageTable
	(let ((entry (rfc1514-storage-table-entry (pop subids))))
	  (case (pop subids)
	    ((1))				      ;hrStorageIndex
	    ((2))				      ;hrStorageType
	    ((3))				      ;hrStorageDescr
	    ((4))				      ;hrStorageAllocationUnits
	    ((5))				      ;hrStorageSize
	    ((6))				      ;hrStorageUsed
	    ((7)))))))				      ;hrStorageAllocationFailures

    ((3)					      ;hrDevice
     (case (pop subids)
       ((1))					      ;hrDeviceTypes
       ((2))					      ;hrDeviceTable
       ((3))					      ;hrProcessorTable
       ((4))					      ;hrNetworkTable
       ((5))					      ;hrPrinterTable
       ((6))					      ;hrDiskStorageTable
       ((7))					      ;hrPartitionTable
       ((8))))					      ;hrFSTable

    ((4)					      ;hrSWRun
     (case (pop subids)
       ((1)					      ;hrSWOSIndex
	(and (= (pop subids) 0) 0))
       ((2)					      ;hrSWRunTable
	(let ((entry (rfc1514-sw-run-entry (pop subids))))
	  (case (pop subids)
	    ((1)				      ;hrSWRunIndex
	     (and (= (pop subids) 0)
		  (rfc1514-sw-run-index entry)))
	    ((2)				      ;hrSWRunName
	     (and (= (pop subids) 0)
		  (rfc1514-sw-run-name entry)))
	    ((3))				      ;hrSWRunID
	    ((4))				      ;hrSWRunPath
	    ((5))				      ;hrSWRunParameters
	    ((6))				      ;hrSWRunType
	    ((7)))))))				      ;hrSWRunStatus

    ((5)					      ;hrSWRunPerf
     (case (pop subids)
       ((1)					      ;hrSWRunPerfTable
	(let ((entry (rfc1514-sw-run-perf-entry (pop subids))))
	  (case (pop subids)
	    ((1))				      ;hrSWRunPerfCPU
	    ((2)))))))				      ;hrSWRunPerfMem

    ((6)					      ;hrSWInstalled
     (case (pop subids)
       ((1))					      ;hrSWInstalledLastChange
       ((2))					      ;hrSWInstalledLastUpdateTime
       ((3)					      ;hrSWInstalledTable
	(let ((entry (rfc1514-sw-installed-entry (pop subids))))
	  (case (pop subids)
	    ((1))				      ;hrSWInstalledIndex
	    ((2))				      ;hrSWInstalledName
	    ((3))				      ;hrSWInstalledID
	    ((4))				      ;hrSWInstalledType
	    ((5)))))))))			      ;hrSWInstalledDate

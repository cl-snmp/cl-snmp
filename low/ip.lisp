;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  ip.lisp
;;; Description:  Internet Interface
;;; Author:	  Simon Leinen (simon@liasun1)
;;; Date Created: 30-May-92
;;; RCS $Header: /usr/people/simon/cl/sysman/RCS/ip.lisp,v 1.13 1998/07/12 22:12:47 leinen Exp simon $  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package "SNMP")

(deftype ipaddr () '(unsigned-byte 32))
(deftype tcp/udp-port () '(unsigned-byte 16))

;; Under CMU CL, we use the standard definitions in internet.lisp.
#-CMU
(progn
  (defconstant af-unix 1)
  (defconstant af-inet 2)

  (defconstant sock-stream #-SVR4 1 #+SVR4 2)
  (defconstant sock-dgram  #-SVR4 2 #+SVR4 1))

(defstruct (network-address
	    (:copier nil)
	    (:predicate nil)
	    (:print-function print-network-address)
	    (:constructor make-network-address (ip-address port)))
  (ip-address nil :type (or null ipaddr))
  (port nil :type (or null tcp/udp-port))
  #+(and ExCL (not (version>= 5 0)))
  (%cstruct nil :type (or null (unsigned-byte 32)))
  #+CMU
  (%address-buffer nil :type (or null system:system-area-pointer))
  (length 16))

(declaim
 (ftype (function ((or null base-string ipaddr) (or null tcp/udp-port)) network-address)
	host/port->network-address)
 (ftype (function (ipaddr (or null tcp/udp-port)) network-address)
	addr/port->network-address)
 (ftype (function ((or null base-string ipaddr)) ipaddr)
	hostname->ip-address)
 (ftype (function (base-string) ipaddr)
	hostname->ip-address-1)
 )

;; Hash table for the host/port to network address mapping.
;;
(defvar *addr/port->network-address-table*
  (make-hash-table :test #'equal))

(defun host/port->network-address (host port)
  (addr/port->network-address (hostname->ip-address host) port))

(defun addr/port->network-address (ip-address port)
  (let ((key (cons ip-address port)))
    (or (gethash key *addr/port->network-address-table*)
	(setf (gethash key *addr/port->network-address-table*)
	  (make-network-address
	   ip-address port)))))

;; Hash table for the hostname to IP address mapping.
;;
(defvar *hostname->ip-address-table*
  (make-hash-table :test #'equal))

(defun hostname->ip-address (host)
  (if (typep host 'ipaddr)
      host
    (or (gethash host *hostname->ip-address-table*)
	(setf (gethash host *hostname->ip-address-table*)
	  (if host
	      (hostname->ip-address-1 host)
	    (make-unspecified-ip-address))))))

#+(and ExCL (version>= 5 0))
(progn
  
(defun print-network-address (na stream level)
  (declare (ignore level))
  (let ((ipa (network-address-ip-address na)))
    (format stream "#<Network-Address [~d.~d.~d.~d:~d]>"
	    (logand (ash ipa -24) #xff)
	    (logand (ash ipa -16) #xff)
	    (logand (ash ipa -8) #xff)
	    (logand (ash ipa 0) #xff)
	    (network-address-port na))))

(defun hostname->ip-address-1 (host)
  (socket:lookup-hostname host))

(ff:def-c-type (c-int :in-foreign-space) :int)

(defun make-unspecified-ip-address ()
  0)

(defun bind-socket (socket na)
  (let ((new-socket (socket:make-socket
		     :type (socket:socket-type socket)
		     :format (socket:socket-format socket)
		     :address-family (socket:socket-address-family socket)
		     :local-port (network-address-port na))))
    (close socket)
    new-socket))
);Allegro 5.0 and later

#+(and ExCL (not (version>= 5 0)))
(progn
  
(defun print-network-address (na stream level)
  (declare (ignore level))
  (let ((ipa (network-address-ip-address na)))
    (format stream "#<Network-Address [~d.~d.~d.~d:~d]>"
	    (logand (ash ipa -24) #xff)
	    (logand (ash ipa -16) #xff)
	    (logand (ash ipa -8) #xff)
	    (logand (ash ipa 0) #xff)
	    (network-address-port na))))

(ff:def-c-type (c-int :in-foreign-space) :int)

(defun network-address-cstruct (na)
  (or (network-address-%cstruct na)
      (setf (network-address-%cstruct na)
	(let ((cstruct (ff:malloc-cstruct 'ipc::sockaddr-in)))
	  #+(and allegro (version>= 4 3)
		 (not sgi))		;somehow this generates segfaults
	  (excl::memset cstruct 0 (ff::cstruct-length 'ipc::sockaddr-in))
	  #-(and allegro (version>= 4 3)) (ipc::bzero cstruct 16)
	  (setf (si:memref-int cstruct 0 0 :signed-word) af-inet)
	  (when (network-address-port na)
	    (setf (si:memref-int cstruct 2 0 :unsigned-word) 
		  (network-address-port na)))
	  (when (network-address-ip-address na)
	    (setf (si:memref-int cstruct 4 0 :unsigned-long)
	      (network-address-ip-address na)))
	  ;;#-(and allegro (version>= 4 3)) (excl::memset (ipc::sockaddr-in-zero cstruct 0) 0 8)
	  ;;#-(and allegro (version>= 4 3)) (ipc::bzero (ipc::sockaddr-in-zero cstruct 0) 8)
	  cstruct))))

(defun free-network-address (na)
  (when na
    (let ((cstruct (network-address-%cstruct na)))
      (when cstruct
	(ff:free-cstruct cstruct)
	(setf (network-address-%cstruct na) nil)))))

(defun hostname->ip-address-1 (host)
  (labels ((no-host-error ()
	     (error 'unknown-host-error
		    :host-name host))
	   (no-address-error ()
	     (error 'no-address-error
		    :host-name host
		    :address-family af-inet)))
    (let ((hostent 
	   #+(and allegro (version>= 4 3)) (excl::gethostbyname host)
	   #-(and allegro (version>= 4 3)) (ipc::gethostbyname host)))
      (when (zerop hostent)
	(no-host-error))
      (unless (= (ipc::hostent-addrtype hostent) 2)
	(no-address-error))
      (assert (= (ipc::hostent-length hostent) 4))
      (let ((addr (ipc::hostent-addr hostent)))
	(when (or (member comp::.target.
			  '(:hp :sgi4d :sony :dec3100)
			  :test #'eq)
		  (probe-file "/lib/ld.so"))
	  ;; BSD 4.3 based systems require an extra indirection
	  (setq addr (si:memref-int addr 0 0 :unsigned-long)))
	(si:memref-int addr 0 0 :unsigned-long)))))

(defun make-unspecified-ip-address ()
  0)

(defun bind-socket (socket na)
  (let ((result (bind socket 
		      (network-address-cstruct na)
		      (network-address-length na))))
    (if (< result 0)
	(unix-error 'bind-failed-error)
      socket)))
);#+ExCL before 5.0

#+CMU
(progn

(defun print-network-address (na stream level)
  (declare (ignore level))
  #+CMU
  (print-unreadable-object (na stream :identity t :type t)
    (format stream "#<~8,'0x:~d>"
	    (network-address-ip-address na)
	    (network-address-port na))))

(defmacro define-sap-allocator (name size)
  (let ((resource-var-name (intern (format nil "*~A-RESOURCE*" name)
				   (symbol-package name)))
	(allocator-name (intern (format nil "GET-~A" name)
				(symbol-package name)))
	(deallocator-name (intern (format nil "FREE-~A" name)
				  (symbol-package name))))
    `(progn
       (defvar ,resource-var-name '())
       (defun ,allocator-name ()
	 (if (endp ,resource-var-name)
	     (system:allocate-system-memory ,size)
	   (pop ,resource-var-name)))
       (defun ,deallocator-name (sap)
	 (push sap ,resource-var-name)))))
(define-sap-allocator sockaddr-in-sap 16)
(define-sap-allocator int-sap 4)
(define-sap-allocator packet-buffer-sap 8192)

(defun free-network-address (na)
  (when na
    (let ((address-buffer (network-address-%address-buffer na)))
      (when address-buffer
	(free-sockaddr-in-sap address-buffer)
	(setf (network-address-%address-buffer na) nil)))))

(defun network-address-address-buffer (na)
  (or (network-address-%address-buffer na)
      (setf (network-address-%address-buffer na)
	(let ((address-buffer (get-sockaddr-in-sap)))
	  (dotimes (index 16)
	    (setf (system:sap-ref-8 address-buffer index) 0))
	  (setf (system:sap-ref-16 address-buffer 0)
	    af-inet)
	  (when (network-address-port na)
	    (setf (system:sap-ref-16 address-buffer 2)
		  (network-address-port na)))
	  (when (network-address-ip-address na)
	    (setf (system:sap-ref-32 address-buffer 4)
		  (network-address-ip-address na)))
	  address-buffer))))

(defun bind-socket (socket na)
  (let ((result (unix:unix-bind
		 socket
		 (network-address-address-buffer na)
		 (network-address-length na))))
    (if (< result 0)
	(unix-error 'bind-failed-error)
      socket)))

(defun hostname->ip-address-1 (host)
  (labels ((no-host-error ()
	     (error 'unknown-host-error
		    :host-name host))
	   (no-address-error ()
	     (error 'no-address-error
		    :host-name host
		    :address-family af-inet)))
    (let ((hostent (ext:lookup-host-entry host)))
      (unless hostent
	(no-host-error))
      (when (endp (ext:host-entry-addr-list hostent))
	(no-address-error))
      (first (ext:host-entry-addr-list hostent)))))

(defun make-unspecified-ip-address ()
  0)

);;#+CMU

(define-condition host-error (error)
  ((host-name :initarg :host-name :reader host-error-host-name)))

(define-condition unknown-host-error (host-error)
  ()
  (:report (lambda (c stream)
	     (format stream "Unknown host ~S" (host-error-host-name c)))))

(define-condition no-address-error (host-error)
  ((address-family :initarg :address-family
		   :reader no-address-error-address-family))
  (:report (lambda (c stream)
	     (format stream "Host ~S has no Internet address"
		     (host-error-host-name c)))))

#-Genera
(define-condition unix-error (error)
  ((unix-error-message :initarg :unix-error-message
		       :reader unix-error-message))
  (:report (lambda (c stream)
	     (format stream "Unix error: ~A" (unix-error-message c)))))

#-Genera
(define-condition bind-failed-error (unix-error)
  ()
  (:report (lambda (c stream)
	     (format stream "Bind failed: ~A" (unix-error-message c)))))

#+CMU
(defun unix-error (type &rest args)
  (apply #'error type
	 :unix-error-message (unix:get-unix-error-msg)
	 args))

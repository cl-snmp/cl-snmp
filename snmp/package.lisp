;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  snmp-package.lisp
;;; Description:  Declaration of the SNMP Package
;;; Author:	  Simon Leinen (simon@liasun1)
;;; Date Created: 30-May-92
;;; RCS $Header: /project/cl-snmp/cvsroot/cl-snmp/snmp/package.lisp,v 1.12 2006/03/22 22:05:20 sleinen Exp $  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#-Genera (in-package :cl-user)
#+Genera (in-package :future-common-lisp-user)

(defpackage :snmp
  (:use #+Genera :future-common-lisp
	#-Genera :common-lisp
	:asn.1)
  #+cmu (:import-from ext
		      af-inet af-unix 
		      sock-dgram sock-stream)
  (:export ipaddr tcp/udp-port
	   snmp-get snmp-get-next snmp-set
	   snmp-map-table
	   with-snmp-session
	   decode-pdu encode-pdu
	   print-oid-with-mib
	   *snmp-readtable*
	   *mib*
	   def-scalar-variable def-list-based-mib-table
	   run-agent))

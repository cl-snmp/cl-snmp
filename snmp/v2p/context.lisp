;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  snmpv2p.lisp
;;; Description:  SNMP v2 Contexts
;;; Author:	  Simon Leinen (simon@lia.di.epfl.ch)
;;; Date Created: 19-Jul-95
;;; RCS $Header: /home/leinen/cl/sysman/snmp/v2p/RCS/context.lisp,v 1.2 1999/01/31 22:57:18 simon Exp leinen $
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :snmp)

(defstruct (snmpv2p-context
	     (:copier nil)
	     (:predicate nil))
  (oid))


(defparameter context-zeus-1
  (make-snmpv2p-context
   :oid (make-object-id '(1 3 6 1 6 3 3 1 4 128 178 155 44 1))))

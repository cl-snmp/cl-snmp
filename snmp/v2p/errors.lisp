;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  snmpv2p-errors.lisp
;;; Description:  Condition definitions for Snmpv2p
;;; Author:	  Simon Leinen (simon@lia.di.epfl.ch)
;;; Date Created: 19-Jul-95
;;; RCS $Header: /home/leinen/cl/sysman/snmp/v2p/RCS/errors.lisp,v 1.6 1999/03/28 18:13:05 simon Exp leinen $
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :snmp)

(define-condition snmpv2p-malformed-response-pdu-error (snmp-response-error)
  ()
  (:report (lambda (c stream)
	     (declare (ignore c))
	     (format stream "Malformed Snmpv2p response PDU"))))

(define-condition snmpv2p-illegal-source-party-error (snmp-query-error)
  ()
  (:report (lambda (c stream)
	     (declare (ignore c))
	     (format stream "Source party in PDU must be an Object ID"))))

(define-condition snmpv2p-illegal-destination-party-error (snmp-query-error)
  ()
  (:report (lambda (c stream)
	     (declare (ignore c))
	     (format stream "Destination party in PDU must be an Object ID"))))

(define-condition snmpv2p-unknown-source-party-error (snmp-query-error)
  ()
  (:report (lambda (c stream)
	     (declare (ignore c))
	     (format stream "Source party unknown"))))

(define-condition snmpv2p-unknown-destination-party-error (snmp-query-error)
  ()
  (:report (lambda (c stream)
	     (declare (ignore c))
	     (format stream "Destination party unknown"))))

(define-condition snmpv2p-party-mismatch (snmp-query-error)
  ((priv-party :initarg :priv-party
	       :reader snmpv2p-party-mismatch-priv-party)
   (auth-party :initarg :auth-party
	       :reader snmpv2p-party-mismatch-auth-party))
  (:report (lambda (c stream)
	     (format stream 
		     "Mismatch between party in snmpPriv (~S) and snmpAuth (~S)"
		     (snmpv2p-party-mismatch-priv-party c)
		     (snmpv2p-party-mismatch-auth-party c)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  v2-read-party.lisp
;;; Description:  Read CMU Snmpv2p party.conf file
;;; Author:	  Simon Leinen (simon@lia.di.epfl.ch)
;;; Date Created: 25-Jul-95
;;; RCS $Header: /home/leinen/cl/sysman/snmp/v2p/RCS/read-party.lisp,v 1.3 1999/01/31 22:57:35 simon Exp leinen $
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :snmp)

(defun read-party-definitions (&optional (filename "/etc/party.conf") 
					 (db *parties*))
  (with-open-file (source filename)
    (read-party-definitions-1 db source filename 1 '())))

(defun read-party-definitions-1 (db stream filename lineno parties)
  (multiple-value-bind (party lineno)
      (read-party-definition db stream filename lineno)
    (if party
	(read-party-definitions-1
	 db stream filename lineno (cons party parties))
	(nreverse parties))))

(defun read-party-definition (db stream filename lineno)
  (multiple-value-bind (line lineno)
      (read-next-line stream filename lineno)
    (and line (read-party-definition-1 db stream filename lineno line))))

(defun read-next-line (stream filename lineno)  
  (declare (ignore filename))
  (do ((lineno lineno (1+ lineno))
       (line (read-line stream nil nil) (read-line stream nil nil)))
      ((not line) (values nil lineno))
    (when (and (> (length line) 0)
	       (char/= (schar line 0) #\#))
      (return (values (string-trim #.(vector #\Space #\Tab) line)
		      (1+ lineno))))))

(defun read-party-definition-1 (db stream filename lineno line)
  (multiple-value-bind (name oid)
      (read-party-parse-line line 2)
    (setq oid (read-party-parse-oid oid))
    (multiple-value-setq (line lineno)
      (read-next-line stream filename lineno))
    (let ((transport-address
	   (multiple-value-bind (transport-domain ip-address udp-port)
	       (read-party-parse-line line 3)
	     (setq udp-port (parse-integer udp-port))
	     (read-party-make-transport-address
	      transport-domain ip-address udp-port))))
      (multiple-value-setq (line lineno)
	(read-next-line stream filename lineno))
      (multiple-value-bind (auth-type priv-type)
	  (read-party-parse-line line 2)
	(multiple-value-setq (line lineno)
	  (read-next-line stream filename lineno))
	(multiple-value-bind (foo max-pdu-length)
	    (read-party-parse-line line 2)
	  (multiple-value-setq (line lineno)
	    (read-next-line stream filename lineno))
	  (multiple-value-bind (clock)
	      (read-party-parse-line line 1)
	    (multiple-value-setq (line lineno)
	      (read-next-line stream filename lineno))
	    (let ((auth
		   (multiple-value-bind (auth-private auth-public)
		       (read-party-parse-line line 2)
		     (read-party-make-auth
		      auth-type auth-private auth-public))))
	      (multiple-value-setq (line lineno)
		(read-next-line stream filename lineno))
	      (let ((priv
		     (multiple-value-bind (priv-private priv-public)
			 (read-party-parse-line line 2)
		       (read-party-make-priv
			priv-type priv-private priv-public))))
		(values (add-named-snmpv2p-party
			 (make-snmpv2p-party
			  :name name
			  :oid oid
			  :transport-address transport-address
			  :auth auth :priv priv)
			 db)
			lineno)))))))))

(defun read-party-parse-line (line nfields)
  (read-party-parse-line-1 line nfields '()))

(defun read-party-whitespace-p (char)
  (or (char= char #\Space)
      (char= char #\Tab)))

(defun read-party-parse-line-1 (line nfields fields)
  (let ((position (position-if #'read-party-whitespace-p line)))
    (cond (position
	   (read-party-parse-line-1
	    (subseq line (or (position-if-not #'read-party-whitespace-p line
					      :start (1+ position))
			     (1+ position)))
	    (1- nfields)
	    (cons (subseq line 0 position) fields)))
	  ((> (length line) 0)
	   (apply #'values (nreverse (cons line fields))))
	  (t (apply #'values (nreverse fields))))))

(defun read-party-make-auth (type private public)
  (declare (ignore public))
  (ecase (intern (string-upcase type) (find-package :keyword))
    ((:noauth)
     (make-snmpv2p-no-auth))
    ((:snmpv2md5auth)
     (make-snmpv2p-md5-auth :private private))))

(defun read-party-make-priv (type private public)
  (declare (ignore public private))
  (ecase (intern (string-upcase type) (find-package :keyword))
    ((:nopriv)
     (make-snmpv2p-no-priv))))

(defun read-party-make-transport-address (type ip-address udp-port)
  (ecase (intern (string-upcase type) (find-package :keyword))
    ((:snmpudpdomain)
     (make-snmpv2p-udp-transport-address
      :hostname nil
      :port udp-port))))

(defun read-party-parse-oid (string)
  (let ((subids (multiple-value-list
		 (read-party-parse-line
		  (substitute #\Space #\. string) 1))))
    (assert (string= (pop subids) ""))
    (let ((*read-base* 10))
      (setq subids (mapcar #'parse-integer subids)))
    (make-object-id subids)))

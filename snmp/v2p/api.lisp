;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  snmpv2p.lisp
;;; Description:  SNMP v2
;;; Author:	  Simon Leinen (simon@lia.di.epfl.ch)
;;; Date Created: 17-Jul-95
;;; RCS $Header: /home/leinen/cl/sysman/snmp/v2p/RCS/api.lisp,v 1.10 1999/01/31 22:57:27 simon Exp leinen $
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :snmp)

(defconstant snmp-version-2p	-1)

(defstruct (snmpv2p-session
	     (:include snmp-session)
	     (:copier nil)
	     (:predicate nil))
  (local-party nil :type snmpv2p-party)
  (remote-party nil :type snmpv2p-party)
  (context))

(defmethod snmp-session-snmp-version ((s snmpv2p-session))
  (declare (ignore s))
  snmp-version-2)

(defun open-snmpv2p-session (mgmt-party agent-party context)
  (setq mgmt-party (coerce-to-party mgmt-party)
	agent-party (coerce-to-party agent-party))
  (let ((session (make-snmpv2p-session 
		  :local-party mgmt-party
		  :remote-party agent-party
		  :context context)))
    (let ((local-party (snmpv2p-session-local-party session))
	  (remote-party (snmpv2p-session-remote-party session)))
      (setf (udp-session-remote-host session)
	(snmpv2p-udp-transport-address-hostname
	 (snmpv2p-party-transport-address remote-party)))
      (setf (udp-session-remote-port session)
	(snmpv2p-udp-transport-address-port
	 (snmpv2p-party-transport-address remote-party)))
      (setf (udp-session-local-host session)
	(snmpv2p-udp-transport-address-hostname
	 (snmpv2p-party-transport-address local-party)))
      (setf (udp-session-local-port session)
	(snmpv2p-udp-transport-address-port
	 (snmpv2p-party-transport-address local-party))))
    (initialize-udp-session session)
    session))

(defmethod query->response ((s snmpv2p-session) query equalp)
  (snmp-send-query s query)
  (loop
   (handler-case
    (multiple-value-bind (response-pdu start end responder-address)
	(snmp-receive-response s)
      (return
	(and response-pdu
	     (let ((response (decode-pdu response-pdu :start start :end end)))
	       (unless (and (typep response 'typed-asn-tuple)
			    (= (typed-asn-tuple-type response) 129)
			    (= (length (typed-asn-tuple-elements response)) 2))
		 (signal 'snmpv2p-malformed-response-pdu-error
			 :query query :response response))
	       (let ((dst-party-oid (first (typed-asn-tuple-elements response)))
		     (priv-data (second (typed-asn-tuple-elements response)))
		     priv-dst-party)
		 (unless (and (typep dst-party-oid 'object-id))
		   (signal 'snmpv2p-illegal-destination-party :query response))
		 (unless (setq priv-dst-party (oid->party dst-party-oid))
		   (signal 'snmpv2p-unknown-destination-party :query response))
		 (setq priv-data
		       (decode-snmp-priv-msg
			(snmpv2p-party-priv priv-dst-party)
			priv-data))
		 (let ((auth-msg (decode-pdu-from-context priv-data)))
		   (unless (and (typep auth-msg 'typed-asn-tuple)
				(= (typed-asn-tuple-type auth-msg) 129)
				(= (length (typed-asn-tuple-elements auth-msg)) 2))
		     (signal 'snmpv2p-malformed-snmp-auth-msg
			     :query response))
		   (let ((auth-info (first (typed-asn-tuple-elements auth-msg)))
			 (mgmt-com (second (typed-asn-tuple-elements auth-msg))))
		     (unless (and (typep mgmt-com 'typed-asn-tuple)
				  (= (typed-asn-tuple-type mgmt-com) 130)
				  (= (length (typed-asn-tuple-elements mgmt-com)) 4))
		       (signal 'snmpv2p-malformed-snmp-mgmt-com
			       :query response))
		     (let ((dst-party-oid
			    (first (typed-asn-tuple-elements mgmt-com)))
			   (src-party-oid
			    (second (typed-asn-tuple-elements mgmt-com)))
			   (context-oid
			    (third (typed-asn-tuple-elements mgmt-com)))
			   (pdu (fourth (typed-asn-tuple-elements mgmt-com)))
			   dst-party src-party)
		       (unless (setq dst-party (oid->party dst-party-oid))
			 (signal 'snmpv2p-unknown-destination-party 
				 :query response))
		       (unless (eq dst-party priv-dst-party)
			 (signal 'snmpv2p-party-mismatch
				 :query response
				 :priv-party priv-dst-party
				 :auth-party dst-party))
		       (unless (setq src-party (oid->party src-party-oid))
			 (signal 'snmpv2p-unknown-source-party
				 :query response))
		       (unless (check-authentication
				(snmpv2p-party-auth src-party)
				auth-info
				mgmt-com)
			 (signal 'snmpv2p-authentication-failure
				 :query :response))
		       (fourth (typed-asn-tuple-elements pdu))))))))))
    (snmp-response-id-mismatch-error (c)
				     (declare (ignore c))))))

(defun encode-snmp-mgmt-com (local remote context pdus)
  (make-typed-asn-tuple 130 (list* (snmpv2p-party-oid remote)
				   (snmpv2p-party-oid local)
				   (snmpv2p-context-oid context)
				   pdus)))

(defmethod snmp-request ((s snmpv2p-session) req bindings &optional (equalp nil))
  (query->response s (make-snmp-query s req bindings 0 0) equalp))

(defmethod snmp-bulk-request ((s snmpv2p-session) req bindings non-rep max-rep)
  (query->response s (make-snmp-query s req bindings non-rep max-rep) nil))

(defmethod make-snmp-query ((s snmpv2p-session) request-code bindings i0 i1)
  (encode-pdu
   (encode-snmp-priv-msg
    (snmpv2p-party-priv (snmpv2p-session-remote-party s))
    (snmpv2p-session-remote-party s)
    (encode-snmp-auth-msg
     (snmpv2p-party-auth (snmpv2p-session-local-party s))
     (encode-snmp-mgmt-com
      (snmpv2p-session-local-party s)
      (snmpv2p-session-remote-party s)
      (snmpv2p-session-context s)
      (list (make-typed-asn-tuple
	     request-code
	     (list (snmp-session-next-request-id s) i0 i1
		   bindings))))))))

(defun decode-pdu-from-context (pdu)
  (let ((*application-specific-ber-decoder* #'ber-decode-snmp-objects))
    (decode-from-context pdu)))

(defun snmp-get-bulk (session attributes non-rep max-rep)
  (snmp-bulk-request
   session pdu-type-get-bulk-request
   (mapcar #'(lambda (attribute) (list attribute nil)) attributes)
   non-rep max-rep))

(defmethod snmp-map-tree ((session snmpv2p-session) attributes function)
  (let ((bindings (mapcar #'(lambda (attribute) (list attribute nil))
			  attributes)))
    (loop
     (handler-case
      (setq bindings
	    (snmp-bulk-request session pdu-type-get-bulk-request bindings
			       0 100))
      (snmp-no-such-name-error (c)
			       (declare (ignore c))
			       (return))
      (snmp-generic-error (c)
			  (setf (cdar bindings) c)
			  (return)))
     (unless bindings
       (error "no response from SNMP service on ~S" session))
     (if (do ((attrs attributes (rest attrs))
	      (bindings bindings (rest bindings)))
	     ((endp bindings) nil)
	   ;; the logic here is not quite correct!!
	   (unless (oid-prefix-p (first attributes) (first (first bindings)))
	     (return t))
	   (when (endp attrs)
	     (apply function bindings)
	     (setq attrs attributes)))
	 (return)
	 (progn
	   (apply function bindings))))))

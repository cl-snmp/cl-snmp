;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  snmpv2p-party.lisp
;;; Description:  SNMP v2 Parties
;;; Author:	  Simon Leinen (simon@lia.di.epfl.ch)
;;; Date Created: 19-Jul-95
;;; RCS $Header: /home/leinen/cl/sysman/snmp/v2p/RCS/party.lisp,v 1.8 1999/01/31 22:57:25 simon Exp leinen $
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :snmp)

(defstruct (snmpv2p-party
	     (:predicate nil)
	     (:copier nil)
	     (:print-function print-snmpv2p-party))
  (transport-address
   nil :type snmpv2p-transport-address)
  (auth nil :type (or null snmpv2p-auth))
  (priv nil :type (or null snmpv2p-priv))
  (oid nil :type (or null object-id))
  (name nil :type (or null base-string)))

(defun print-snmpv2p-party (party stream level)
  (declare (ignore level))
  (print-unreadable-object (party stream :type t :identity t)
    (prin1 (snmpv2p-party-name party) stream)))

(defstruct (snmpv2p-auth
	     (:predicate nil)
	     (:copier nil)))

(defstruct (snmpv2p-priv
	     (:predicate nil)
	     (:copier nil)))

(defstruct (snmpv2p-transport-address
	     (:predicate nil)
	     (:copier nil)))

(defstruct (snmpv2p-udp-transport-address
	     (:include snmpv2p-transport-address)
	     (:predicate nil)
	     (:copier nil))
  (hostname nil :type (or null base-string))
  (port nil :type (or null (unsigned-byte 16)))
  #+CMU (address-buffer (get-sockaddr-in-sap))
  #+(and excl (not (version>= 5 0))) (address-buffer (ff:malloc-cstruct 'ipc::sockaddr-in)))

(defstruct (party-data-base
	     (:copier nil)
	     (:predicate nil)
	     (:constructor make-party-data-base ()))
  (keyword->party-list '() :type list)
  (object-id->party-list '() :type list))

(defvar *parties*)

(setq *parties* (make-party-data-base))

(defvar *keyword->party-list* '())
(defvar *object-id->party-list* '())

(defun add-named-snmpv2p-party (party db)
  (let ((name (snmpv2p-party-name party))
	(object-id (snmpv2p-party-oid party)))
    (when name
      (push (cons (intern (string-upcase name) (find-package :keyword))
		  party)
	    (party-data-base-keyword->party-list db)))
    (when object-id
      (push party (party-data-base-object-id->party-list db)))
    party))

(defmethod coerce-to-party ((p snmpv2p-party)) p)
(defmethod coerce-to-party ((s string))
  (keyword->party (intern (string-upcase s) (find-package :keyword))))
(defmethod coerce-to-party ((s symbol))
  (assert (keywordp s))
  (keyword->party s))
(defmethod coerce-to-party ((o object-id))
  (oid->party o))

(defun keyword->party (k &optional (db *parties*))
  (cdr (assoc k (party-data-base-keyword->party-list db) :test #'eq)))

(defun oid->party (oid &optional (db *parties*))
  (find oid (party-data-base-object-id->party-list db)
	:test #'oid-equal :key #'snmpv2p-party-oid))

;;;; noPriv
(defstruct (snmpv2p-no-priv
	     (:include snmpv2p-priv)
	     (:predicate nil)
	     (:copier nil)))

(defstruct (typed-string
	     (:copier nil)
	     (:predicate nil)
	     (:constructor make-typed-string (type string)))
  (type)
  (string))

(defmethod ber-encode-snmp-objects (buffer (s typed-string))
  (asn.1::ber-write-octet-string 
   buffer (typed-string-string s) (typed-string-type s)))

(defmethod encode-snmp-priv-msg ((priv snmpv2p-no-priv) dest auth-msg)
  (make-typed-asn-tuple 129 (list (snmpv2p-party-oid dest)
				  (make-typed-string 
				   129
				   (encode-pdu auth-msg)))))

(defmethod decode-snmp-priv-msg ((priv snmpv2p-no-priv) encrypted)
  encrypted)

;;(defstruct (snmpv2p-des-priv
;;	       (:include snmpv2p-priv)
;;	       (:predicate nil)
;;	       (:copier nil))
;;  (private nil :type (or null base-string)))

;;;; noAuth

(defstruct (snmpv2p-no-auth
	     (:include snmpv2p-auth)
	     (:predicate nil)
	     (:copier nil)))

(defmethod encode-snmp-auth-msg ((auth snmpv2p-no-auth) mgmt-com)
  (make-typed-asn-tuple 129 (list "" mgmt-com)))

(defmethod check-authentication ((auth snmpv2p-no-auth) info data)
  (declare (ignore data))
  (and (typep info 'base-string) (= (length info) 0)))

(defstruct (snmpv2p-md5-auth
	       (:include snmpv2p-auth)
	       (:predicate nil)
	       (:copier nil))
  (private nil :type (or null base-string)))

;;;; Example parties

(add-named-snmpv2p-party
 (make-snmpv2p-party
  :name "zeusAgent"
  :oid (make-object-id '(1 3 6 1 6 3 3 1 3 128 178 155 44 1))
  :transport-address (make-snmpv2p-udp-transport-address
		      :hostname "liasg7" :port 161)
  :auth (make-snmpv2p-no-auth)
  :priv (make-snmpv2p-no-priv))
 *parties*)

(add-named-snmpv2p-party
 (make-snmpv2p-party
  :name "zeusMS"
  :oid (make-object-id '(1 3 6 1 6 3 3 1 3 128 178 155 44 2))
  :transport-address (make-snmpv2p-udp-transport-address
		      :hostname nil :port nil)
  :auth (make-snmpv2p-no-auth)
  :priv (make-snmpv2p-no-priv))
 *parties*)

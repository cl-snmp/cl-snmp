;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  snmp-netstat.lisp
;;; Description:  Netstat implementation using SNMP
;;; Author:	  Simon Leinen (simon@liasun2)
;;; Date Created: 31-May-92
;;; RCS $Header: /home/leinen/CVS/lisp-snmp/snmp/netstat.lisp,v 1.12 2003/12/29 10:14:57 leinen Exp $  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :snmp)

(eval-when (compile eval)
  (setq *readtable* *snmp-readtable*))

(eval-when (compile load eval)
  
  (defconstant tcp-connection-state-closed 1)
  (defconstant tcp-connection-state-listen 2)
  (defconstant tcp-connection-state-syn-sent 3)
  (defconstant tcp-connection-state-syn-received 4)
  (defconstant tcp-connection-state-established 5)
  (defconstant tcp-connection-state-fin-wait-1 6)
  (defconstant tcp-connection-state-fin-wait-2 7)
  (defconstant tcp-connection-state-close-wait 8)
  (defconstant tcp-connection-state-last-ack 9)
  (defconstant tcp-connection-state-closing 10)
  (defconstant tcp-connection-state-time-wait 11)
  (defconstant tcp-connection-state-delete-tcb 12))

(defun tcp-connection-state-name (tcp-connection-state)
  (aref #("illegal(0)" "closed" "listen" "synSent" "synReceived"
	  "established" "finWait1" "finWait2" "closeWait" "lastAck"
	  "closing" "timeWait" "deleteTCB")
	tcp-connection-state))

(defun interesting-peer-p (ip-address)
  ;; loopback connections are uninteresting
  (not (equal ip-address '(127 0 0 1))))

(defun snmp-netstat (host &optional (stream t))
  (snmp-map-table
   host
   '([tcpConnState])
   #'(lambda (index state)
       (multiple-value-bind
	   (local-address local-port
	    remote-address remote-port)
	   (index->connection index)
	 (when
	     (and (member state '(#.tcp-connection-state-syn-sent
				  #.tcp-connection-state-syn-received
				  #.tcp-connection-state-established
				  #.tcp-connection-state-fin-wait-1
				  #.tcp-connection-state-fin-wait-2
				  #.tcp-connection-state-close-wait
				  #.tcp-connection-state-last-ack
				  #.tcp-connection-state-closing
				  #.tcp-connection-state-time-wait
				  #.tcp-connection-state-delete-tcb))
		  (interesting-peer-p remote-address))
	   (format stream "~&~22@A:~16A ~22@A:~16A ~A"
		   (ip-address->string local-address) 
		   (tcp-port->string local-port)
		   (ip-address->string remote-address) 
		   (tcp-port->string remote-port)
		   (tcp-connection-state-name state)))))))

(defun index->connection (index)
  (let (local-address local-port remote-address remote-port)
    (setq local-address (subseq index 0 4))
    (setq index (cddddr index))
    (setq local-port (pop index))
    (setq remote-address (subseq index 0 4))
    (setq index (cddddr index))
    (setq remote-port (pop index))
    (values local-address local-port remote-address remote-port)))

(defvar *inverse-mapping-cache*
    (make-hash-table))

(defun ip-address-list->number (addr)
  (reduce #'(lambda (x y)
	      (declare (type (unsigned-byte 8) y)
		       (type (unsigned-byte 24) x))
	      (+ (ash x 8) y))
	  addr))

#+CMU
(defun ip-address->hostname (addr)
  (etypecase addr
    ((unsigned-byte 32)
     (let ((cache-entry (gethash addr *inverse-mapping-cache* :foo)))
       (if (eq cache-entry :foo)
	   (let ((hostent (ext:lookup-host-entry addr)))
	     (setf (gethash addr *inverse-mapping-cache*)
	       (and hostent
		  (ext:host-entry-name hostent))))
	 cache-entry)))
    (list
     (ip-address->hostname (ip-address-list->number addr)))))

#+(and excl (version>= 5 0))
(defun ip-address->hostname (addr)
  (etypecase addr
    ((unsigned-byte 32)
     (let ((cache-entry (gethash addr *inverse-mapping-cache* :foo)))
       (if (eq cache-entry :foo)
	   (let ((hostent (socket:ipaddr-to-hostname addr)))
	     (setf (gethash addr *inverse-mapping-cache*)
	       hostent))
	 cache-entry)))
    (list
     (ip-address->hostname (ip-address-list->number addr)))))

#+(and excl (not (version>= 5 0)))
(defun ip-address->hostname (addr)
  (let ((result (gethostbyaddr (make-array (list 4)
					   :element-type '(unsigned-byte 8)
					   :initial-contents addr)
			       4 ipc::*af-inet*)))
    (and (not (zerop result))
	 (ff:char*-to-string
	  (ipc::hostent-name result)))))

#+Genera
(defun ip-address->hostname (addr)
  (let ((numeric-ip-address (reduce #'(lambda (a b) (+ (* 256 a) b)) addr)))
    (let ((host (net:get-host-from-address 
		 numeric-ip-address
		 (neti:find-network-named :internet)
		 t)))
      (or (and host
	       (or (zl:send host :internet-domain-name)
		   (zl:send host :name)))))))

#-(or CMU excl Genera)
(defun ip-address->hostname (addr)
  nil)

(defun ip-address->string (addr)
  (or (ip-address->hostname addr)
      (format nil "~{~3,'0D~^.~}" addr)))

#+Genera
(defun tcp-port->string (port)
  (or (cdr (assoc port tcp::*tcp-protocol-alist*))
      (format nil "~D" port)))

#-Genera
(defun tcp-port->string (port)
  (format nil "~D" port))

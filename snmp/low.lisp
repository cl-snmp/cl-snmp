;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  snmp-low.lisp
;;; Description:  System-dependent transport layer
;;; Author:	  Simon Leinen (simon@liasun2)
;;; Date Created: 29-May-92
;;; RCS $Header: /home/leinen/cl/sysman/snmp/RCS/low.lisp,v 1.9 2002/08/11 21:53:53 leinen Exp leinen $  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :snmp)

#+excl
(progn

  #+linux
  (load "sysman:src;errno.so")
  #+(and foreign-load-so (not Linux))
  (load ""
	:unreferenced-lib-names '("sendto" "recvfrom" "bind"
				  "setsockopt" "getsockopt"))
  #-(or foreign-load-so linux)
  (load "sysman:src;errno.o"
	:unreferenced-lib-names 
	#+svr4 '("sendto" "recvfrom" "bind"
		 "setsockopt" "getsockopt")
	#-svr4 '("_sendto" "_recvfrom" "_bind"
		 "_setsockopt" "_getsockopt"))

  (ff:defforeign 'sendto
      :arguments '(integer (simple-array (unsigned-byte 8) (*)) integer integer integer integer))
  (ff:defforeign 'recvfrom
      :arguments '(integer (simple-array (unsigned-byte 8) (*)) integer integer integer integer))
  (ff:defforeign 'bind
      :arguments '(integer integer integer))
  (ff:defforeign 'setsockopt
      :arguments '(integer integer integer
		   integer
		   integer))
  (ff:defforeign 'getsockopt
      :arguments '(integer integer integer
		   integer
		   (simple-array (unsigned-byte 32) (*))))
  #-(and svr4 sun)
  (ff:defforeign 'unix-errno
      :entry-point #+(or SVR4 Linux) "unix_errno" #-(or SVR4 Linux) "_unix_errno"
      :arguments '())
  #+(and svr4 sun)
  (ff:defforeign 'unix-___errno
      :entry-point "___errno"
      :arguments '())
  (ff:defforeign 'gethostbyaddr
      :arguments '((simple-array (unsigned-byte 8) (*)) integer integer)
      :return-type :integer)

  #+(and svr4 sun)
  (defun unix-errno ()
    (c-int (unix-___errno))))

#+Genera
(defconstant default-snmp-udp-port 161)

#+Genera
(net:define-protocol :snmp (:network-management :datagram)
  (:invoke (sap)
    (let ((args (neti:service-access-path-args sap)))
      (let ((pdu args))
	(with-open-stream
	    (stream (neti:get-connection-for-service 
		     sap :rfc-data pdu))
	  (multiple-value-bind (datagram start end)
	      (zl:send stream :read-input-buffer)
	    (subseq datagram start end)))))))

#+Genera
(tcp:add-udp-port-for-protocol :snmp default-snmp-udp-port)

/*
 setsockopt.c

 Date Created: Sun Mar 28 16:50:51 1999
 Author:       Simon Leinen  <simon@limmat.switch.ch>
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

int main (int argc, char **argv) {
  printf ("((SOL_SOCKET %d) (SO_RCVBUF %d))\n",
	  (int) SOL_SOCKET, (int) SO_RCVBUF);
  exit (0);
}

cl-snmp
=======

Author: Simon Leinen <simon.leinen@gmail.com>

The code in this directory implements the SNMP (versions 1 and 2"c")
protocol in Common Lisp.  It provides the following functionality:

* Encoding and decoding a subset of ASN.1 using BER (Basic Encoding
  Rules).  The supported subset is sufficient for SNMP.

* Sending SNMP requests (GET, SET, GET-NEXT, GET-BULK) to a remote
  host via UDP, and decoding the responses.

* Symbolics Lisp Machine only: supports a large subset of MIB-2 as an
  agent.  Only read access is supported.

* Interoperability with the IRIX scheme of remote sub agents.  You can
  write a subagent in Lisp that handles a MIB subtree.  The IRIX SNMP
  agent will forward requests for this subtree to your code, and will
  forward your response to the original requestor.  You can have
  multiple such subagents running in the same Lisp image.

The code has been tested under CMU Common Lisp 17f and Allegro Common
Lisp (4.2–9.0) on Sun Solaris, GNU/Linux, and SGI IRIX workstations.
It used to run on Symbolics Lisp Machines running Genera 8.2 as well,
but it's been a long time since I tested that, so I may have broken
something in the meantime.

Acknowledgements
----------------

Håkon Alstadheim contributed a system definition (snmp.asd) that
should enable the package to be compiled using the ASDF system
construction tool.

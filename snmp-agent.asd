;;; -*- mode: lisp -*-
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  snmp.asd
;;; Description:  ASDF System description for SYSMAN
;;; Author:	  Simon Leinen (simon@liasun1)
;;; Date Created: 29-Dec-2003
;;; RCS $Header$
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defsystem snmp-agent
    :components
  ((:system :snmp)
   (:file "agent/generic/mib-instr")
   (:file "agent/generic/errors"
	  :depends-on ("agent/generic/mib-instr"))
   (:file "agent/generic/main"
	  :depends-on ("agent/generic/mib-instr"))
   (:file "agent/generic/mib-2"
	  :depends-on ("agent/generic/mib-instr"))))

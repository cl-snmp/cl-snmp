;;; -*- mode: lisp -*-
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  snmp.asd
;;; Description:  ASDF System description for SYSMAN
;;; Author:	  Simon Leinen (simon@liasun1)
;;; Date Created: 29-Dec-2003
;;; RCS $Header: /project/cl-snmp/cvsroot/cl-snmp/snmp.asd,v 1.1 2003/12/30 13:47:02 leinen Exp $
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defpackage :snmp-system
  (:use :common-lisp :asdf))

(defsystem snmp
    :components
  ((:file "asn1/package" :pathname "asn1/package")
   (:file "snmp/package" :pathname "snmp/package" 
	  :depends-on ("asn1/package"))
   (:file "dependent"
    :in-order-to ((compile-op (load-op "snmp/package")))
    :in-order-to ((load-op (load-op "asn1/package"))))
   (:file "asn1/defs" :pathname "asn1/defs"
    :in-order-to ((compile-op (load-op "asn1/package" "dependent")))
    :in-order-to ((load-op (load-op "asn1/package"))))
   (:file "ber/defs"  :pathname "ber/defs"
    :depends-on ("asn1/package" "asn1/defs"))
   (:file "ber/decode" :pathname "ber/decode"
    :depends-on ("asn1/package" "asn1/defs" "ber/defs"))
   (:file "ber/encode" :pathname "ber/encode"
    :depends-on ("asn1/package" "asn1/defs" "ber/defs"))
   (:file "mib/defs" :pathname "mib/defs"
    :in-order-to ((compile-op (load-op "snmp/package" "dependent"))))
   (:file "mib/read" :pathname "mib/read"
    :in-order-to ((compile-op (load-op "snmp/package" "dependent" "mib/defs"))))
   (:file "snmp/low" :pathname "snmp/low"
    :in-order-to ((compile-op (load-op "snmp/package" "dependent")))
    :in-order-to ((load-op (load-op "snmp/package"))))
   (:file "snmp/pdu" :pathname "snmp/pdu"
    :in-order-to ((compile-op (load-op "snmp/package" "dependent" "snmp/low"))))
   (:file "low/ip" :pathname "low/ip"
    :in-order-to ((compile-op (load-op "snmp/package" "dependent"))))
   (:file "low/udp" :pathname "low/udp"
    :in-order-to ((compile-op (load-op "snmp/package" "dependent" "low/ip"))))
   (:file "snmp/session" :pathname "snmp/session"
    :in-order-to ((compile-op (load-op "snmp/package" "dependent"
				  "low/ip" "low/udp" "snmp/pdu"))))
   (:file "snmp/errors" :pathname "snmp/errors"
    :in-order-to ((compile-op (load-op "snmp/package" "dependent"
				  "low/ip" "low/udp" "snmp/pdu"))))
   (:file "snmp/api" :pathname "snmp/api"
    :depends-on ("snmp/package" "dependent"
			    "low/ip" "low/udp" "snmp/pdu" "snmp/session"))
   (:file "mib/oid-reader" :pathname "mib/oid-reader"
    :in-order-to ((compile-op (load-op "snmp/package" "dependent" "mib/defs")))
    :in-order-to ((load-op (load-op "snmp/package" "asn1/package" "asn1/defs" "mib/defs"))))   
   (:file "mib-2"
    :in-order-to ((compile-op (load-op "snmp/package" "dependent" "mib/defs" "mib/read"))))
   (:file "snmp/netstat" :pathname "snmp/netstat"
    :in-order-to ((compile-op (load-op "snmp/package" "dependent" "snmp/api" "mib/oid-reader" "mib-2"))))
   (:file "test"
    :in-order-to ((compile-op (load-op "snmp/package" "dependent" "snmp/api"))))))

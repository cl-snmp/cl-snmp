;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  mib-2.lisp
;;; Description:  Load the MIB
;;; Author:	  Simon Leinen (simon@liasun2)
;;; Date Created:  2-Jun-92
;;; RCS $Header: /project/cl-snmp/cvsroot/cl-snmp/mib-2.lisp,v 1.5 2006/03/22 22:03:47 sleinen Exp $  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :snmp)

(eval-when (:compile-toplevel :load-toplevel)
  (setq *mib* '#.(read-mib default-mib-pathname
			   :name "mib-2"
			   :pretty-name "MIB-II (RFC 1213)")))

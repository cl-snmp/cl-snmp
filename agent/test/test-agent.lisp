;;; -*- readtable: snmp -*-
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; test-agent.lisp
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Date Created: March 29, 2000
;;; Author:       Simon Leinen  <simon@limmat.switch.ch>
;;; RCS $Id: test-agent.lisp,v 1.2 2002/08/11 21:50:48 leinen Exp leinen $
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :snmp)

(eval-when (compile eval)
  (when (and (not (assoc :snmp excl::*named-readtables*))
	     (boundp '*snmp-readtable*))
    (push (cons :snmp *snmp-readtable*) excl::*named-readtables*))
  (setq *readtable* *snmp-readtable*))

(defclass counter-simulator () ())

(defclass sinoid-counter-simulator (counter-simulator)
  ((max-rate
    :initarg :max-rate
    :accessor cs-max-rate)
   (min-rate
    :initarg :min-rate
    :accessor cs-min-rate)
   (last-value
    :initform (random (ash 1 64))
    :accessor cs-last-value)
   (last-time
    :initform (get-internal-real-time)
    :accessor cs-last-time)))

(defstruct (test-interface
	    (:conc-name tif-))
  (name)
  (snmp-type 1)
  (alias)
  (speed 0)
  (in)
  (out))

(defun make-sin-test-interface (&key
				name
				(snmp-type 1)
				(speed 0)
				alias
				(min-rate-in 0) (max-rate-in 2000000)
				(min-rate-out 0) (max-rate-out 2000000))
  (make-test-interface
   :name name
   :snmp-type snmp-type
   :speed speed
   :alias alias
   :in (make-instance 'sinoid-counter-simulator
	 :min-rate min-rate-in :max-rate max-rate-in)
   :out (make-instance 'sinoid-counter-simulator
	 :min-rate min-rate-out :max-rate max-rate-out)))

(defun tif-hc-in-octets (if)
  (get-simulated-counter-value (tif-in if)))

(defun tif-hc-out-octets (if)
  (get-simulated-counter-value (tif-out if)))

(defun get-simulated-counter-value (sc)
  (let ((now (get-internal-real-time))
	(last (cs-last-time sc)))
    (let ((new (+ (cs-last-value sc)
		  (round
		   (* (- now last)
		      (current-simulated-counter-rate sc)
		      (/ 1 internal-time-units-per-second))))))
      (setf (cs-last-time sc) now)
      (setf (cs-last-value sc) new)
      new)))

(defvar *test-interfaces*)

(setq *test-interfaces*
  (list
   (make-sin-test-interface
    :name "Ethernet0/0"
    :alias "Ethernet to DNS Server"
    :snmp-type 6
    :speed 10000000
    :min-rate-in (round 1000000 8)
    :max-rate-in (round 9000000 8)
    :min-rate-out (round 1000000 8)
    :max-rate-out (round 9000000 8))
   (make-sin-test-interface
    :name "FastEthernet0/1"
    :alias "FastE to FTP Server"
    :snmp-type 62
    :speed 100000000
    :min-rate-in (round 10000000 8)
    :max-rate-in (round 90000000 8)
    :min-rate-out (round 10000000 8)
    :max-rate-out (round 90000000 8))
   (make-sin-test-interface
    :name "GigabitEthernet0/2"
    :snmp-type 117
    :speed 1000000000
    :alias "GigE to UFOO"
    :min-rate-in (round 100000000 8)
    :max-rate-in (round 900000000 8)
    :min-rate-out (round 100000000 8)
    :max-rate-out (round 900000000 8))
   (make-sin-test-interface
    :name "TenGigabitEthernet0/3"
    :alias "10GigE to UBAR"
    :min-rate-in (round 1000000000 8)
    :max-rate-in (round 9000000000 8)
    :min-rate-out (round 1000000000 8)
    :max-rate-out (round 9000000000 8))
   (make-sin-test-interface
    :name "TerabitEthernet1/0"
    :alias "Backup TerE"
    :min-rate-in (round 100000000000 8)
    :max-rate-in (round 900000000000 8)
    :min-rate-out (round 100000000000 8)
    :max-rate-out (round 900000000000 8))
   (make-sin-test-interface
    :name "PetabitEthernet2/0"
    :alias "PetE to MAE North"
    :min-rate-in (round 100000000000000 8)
    :max-rate-in (round 900000000000000 8)
    :min-rate-out (round 100000000000000 8)
    :max-rate-out (round 900000000000000 8))
   (make-sin-test-interface
    :name "ExabitEthernet3/0"
    :alias "ExE to Core"
    :min-rate-in (round 100000000000000000 8)
    :max-rate-in (round 900000000000000000 8)
    :min-rate-out (round 100000000000000000 8)
    :max-rate-out (round 900000000000000000 8))))

(defun current-simulated-counter-rate (sc)
  (cs-max-rate sc))

(def-scalar-variable [sysDescr] (agent)
  "Simon's Virtual Exabit Router")
(def-scalar-variable [sysObjectID] (agent)
    '#.(make-object-id '(1 3 6 1 4 1 2946 2 1 2)))
(def-scalar-variable [sysUpTime] (agent)
    (make-timeticks 
     (truncate (* #.(/ 100 internal-time-units-per-second)
		  (- (get-internal-real-time)
		     (sas-start-up-time agent))))))
(def-scalar-variable [sysName] (agent)
  "exabit-1")

(def-listy-mib-table [ifTable] (:list *test-interfaces*)
  (:entry [ifEntry])
  (:slots
   ([ifIndex] (if ignore subids) (car subids))
   ([ifDescr] (if ignore subids) (tif-name if))
   ([ifType] (if ignore subids) (tif-snmp-type if))
   ([ifSpeed] (if ignore subids) (make-gauge32 (or (tif-speed if) 0)))
   ([ifInOctets] (if ignore subids) (make-counter32 (tif-hc-in-octets if)))
   ([ifOutOctets] (if ignore subids) (make-counter32 (tif-hc-out-octets if)))
   ([ifAdminStatus] (if ignore subids) 1)
   ([ifOperStatus] (if ignore subids) 1)))

(def-listy-mib-table [ifXTable] (:list *test-interfaces*)
  (:entry [ifXEntry])
  (:slots
   ([ifAlias] (if ignore subids) (tif-alias if))
   ([ifHCInOctets] (if ignore subids) (make-counter64 (tif-hc-in-octets if)))
   ([ifHCOutOctets] (if ignore subids) (make-counter64 (tif-hc-out-octets if)))))

(defun start-test-agent (port)
  (let (#+excl (snmp-agent-process nil))
    (when port
      (let ()
	;;(setq *mib* (make-fluxoscope-instrumented-mib fs))
	(setq snmp-agent-process
	  (run-agent :port port))))
    (unwind-protect
	(loop (sleep 60))
      #+excl (when snmp-agent-process
	       (mp:process-kill snmp-agent-process)))))

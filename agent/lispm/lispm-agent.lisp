;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  lispm-agent.lisp
;;; Description:  SNMP Server for Genera
;;; Author:	  Simon Leinen (simon@liasun1)
;;; Date Created: 30-May-92
;;; RCS $Header: /home/leinen/cl/sysman/agent/lispm/RCS/lispm-agent.lisp,v 1.23 1999/03/28 18:09:13 simon Exp leinen $  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :snmp)

(defstruct (snmp-agent-state
	    (:predicate nil)
	    (:copier nil)
	    (:conc-name sas-))
  (start-up-time
   (get-internal-real-time))
  (in-pkts 0 :type (unsigned-byte 32))
  (out-pkts 0 :type (unsigned-byte 32))
  (stats-30-something 0 :type (unsigned-byte 32))
  (in-bad-versions 0 :type (unsigned-byte 32))
  (in-bad-community-names 0 :type (unsigned-byte 32))
  (in-bad-community-uses 0 :type (unsigned-byte 32))
  (in-asn-parse-errs 0 :type (unsigned-byte 32))
  (in-too-bigs 0 :type (unsigned-byte 32))
  (in-no-such-names 0 :type (unsigned-byte 32))
  (in-bad-values 0 :type (unsigned-byte 32))
  (in-read-onlys 0 :type (unsigned-byte 32))
  (in-gen-errs 0 :type (unsigned-byte 32))
  (in-total-req-vars 0 :type (unsigned-byte 32))
  (in-total-set-vars 0 :type (unsigned-byte 32))
  (in-get-requests 0 :type (unsigned-byte 32))
  (in-get-nexts 0 :type (unsigned-byte 32))
  (in-set-requests 0 :type (unsigned-byte 32))
  (in-get-responses 0 :type (unsigned-byte 32))
  (in-traps 0 :type (unsigned-byte 32))
  (out-too-bigs 0 :type (unsigned-byte 32))
  (out-no-such-names 0 :type (unsigned-byte 32))
  (out-bad-values 0 :type (unsigned-byte 32))
  (out-gen-errs 0 :type (unsigned-byte 32))
  (out-get-requests 0 :type (unsigned-byte 32))
  (out-get-nexts 0 :type (unsigned-byte 32))
  (out-set-requests 0 :type (unsigned-byte 32))
  (out-get-responses 0 :type (unsigned-byte 32))
  (out-traps 0 :type (unsigned-byte 32))
  (enable-authen-traps nil :type t))

(defvar *the-snmp-agent*)
(setq *the-snmp-agent* (make-snmp-agent-state))

(net:define-server :snmp (:medium :datagram
				  :request-array (request start end))
  (let ((agent *the-snmp-agent*)
	(neti:*server-debug-flag* t))
    (counter32-incf (sas-in-pkts agent))
    (handler-case
	(let ((request (decode-pdu request :start start :end end)))
	  (cond ((consp request)
		 (counter32-incf (sas-stats-30-something agent))
		 (unless (and (integerp (first request))
			      (stringp (second request))
			      (endp (cdddr request)))
		   (error 'snmpv1-malformed-pdu-error :request pdu))
		 (unless (= (first request) snmp-version-1)
		   (signal 'snmp-bad-version-error
			   :request pdu
			   :requested-version (first request)))
		 (multiple-value-prog1
		     (snmpv1-reply request agent)
		   (counter32-incf (sas-out-pkts agent))))
		((and (typep request 'typed-asn-tuple)
		      (= (typed-asn-tuple-type request) 129))
		 (unless (= (length (typed-asn-tuple-elements request) 2))
		   (signal 'snmpv2-malformed-pdu-error :request pdu))
		 (multiple-value-prog1
		     (snmpv2-reply request agent)
		   (counter32-incf (ses-out-pkts agent))))
		(t (signal 'snmp-malformed-pdu-error
			   :request pdu))))
      (asn.1:asn-error (c)
	(declare (ignore c))
	(counter32-incf (sas-in-asn-parse-errs agent)) nil)
      (snmp-bad-version-error (c)
	(declare (ignore c))
	(counter32-incf (sas-in-bad-versions agent)) nil)
      (snmp-request-bad-community-error (c)
	(declare (ignore c))
	(counter32-incf (sas-in-bad-community-names agent)) nil)
      (snmp-agent-no-such-name-error (c)
	(counter32-incf (sas-in-no-such-names agent))
	(values t (make-error-pdu c))))))

(defmethod make-error-pdu ((c snmp-agent-specific-variable-error))
  (let ((request (snmp-agent-request-error-request c)))
    (let ((snmp-version (pdu-snmp-version request))
	  (snmp-community (pdu-snmp-community request))
	  (type (pdu-type request))
	  (request-id (pdu-request-id request))
	  (bindings (pdu-bindings request)))
    (encode-pdu
     (list snmp-version snmp-community
	   (make-typed-asn-tuple
	    pdu-type-response
	    (list request-id
		  (snmp-agent-specific-variable-error-error-status c)
		  (snmp-agent-specific-variable-error-variable-index c)
		  bindings)))))))

(defun snmpv1-reply (request agent)
  (let ((snmp-version (pdu-snmp-version request))
	(snmp-community (pdu-snmp-community request))
	(type (pdu-type request))
	(request-id (pdu-request-id request))
	(bindings (pdu-bindings request)))
    (cond ((not (eql snmp-version snmp-version-1))
	   (signal 'snmp-bad-version-error
		   :request request :requested-version snmp-version))
	  ((not (equal snmp-community "public"))
	   (signal 'snmp-request-bad-community-error
		   :request request :community snmp-community))
	  (t (let ((new-bindings (snmp-response agent type bindings view response)))
	       (values t
		       (encode-pdu
			(list snmp-version snmp-community
			      (make-typed-asn-tuple 
			       pdu-type-response
			       (list request-id 0 0
				     new-bindings))))))))))

(defun snmp-response (agent type bindings view request)
  (ecase type
    ((#.pdu-type-get-request)
     (counter32-incf (sas-in-get-requests agent))
     (do ((variable-counter 1 (1+ variable-counter))
	  (b bindings (rest b)))
	 ((null b) bindings)
       (let ((binding (first b)))
	 (setf (cadr binding)
	   (or (mib-get-value 
		*mib*
		(object-id-subids
		 (first binding))
		agent view)
	       (signal 'snmp-agent-no-such-name-error
		       :request request
		       :index variable-counter))))))
    ((#.pdu-type-get-next-request)
     (counter32-incf (sas-in-get-nexts agent))
     (let ((variable-counter 0))
       (mapcar #'(lambda (binding)
		   (incf variable-counter)
		   (multiple-value-bind
		       (oid value)
		       (mib-get-next-value
			*mib*
			(object-id-subids
			 (first binding))
			agent
			view)
		     (unless oid
		       (signal 'snmp-agent-no-such-name-error
			       :request request
			       :index variable-counter))
		     (list (make-object-id oid)
			   value)))
	       bindings)))
    ((#.pdu-type-response)
     (counter32-incf (sas-in-get-responses agent)))
    ((#.pdu-type-set-request)
     (counter32-incf (sas-in-set-requests agent)))
    ((#.pdu-type-snmpv1-trap-request)
     (counter32-incf (sas-in-traps agent)))))

;;; -*- Package: SNMP; mode: Common-Lisp -*-
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  lispm-agent-mib-2.lisp
;;; Description:  MIB 2 Instrumentation for Lisp Machines
;;; Author:	  Simon Leinen (simon@lia.di.epfl.ch)
;;; Date Created:  2-Nov-93
;;; RCS $Header: /home/leinen/cl/sysman/agent/lispm/RCS/lispm-agent-mib-2.lisp,v 1.8 1999/03/28 14:39:20 simon Exp leinen $  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :snmp)

(eval-when (compile eval)
  (setq *readtable* *snmp-readtable*))

(def-scalar-variable [sysDescr] (agent)
    (machine-type))
(def-scalar-variable [sysObjectID] (agent)
    '#.(make-object-id '(1 3 6 1 4 1 4 1 2 1 6 9 3 5 2 7 7)))
(def-scalar-variable [sysUpTime] (agent)
    (make-timeticks 
     (truncate (* #.(/ 100 internal-time-units-per-second)
		  (- (get-internal-real-time)
		     (sas-start-up-time *the-snmp-agent*))))))

(def-scalar-variable [sysContact] (agent)
    (or (second (assoc
		 :responsible-person
		 (zl:send net:*local-host* :get :user-property)))
	""))
(def-scalar-variable [sysName] (agent)
    (or (zl:send net:*local-host* :get :pretty-name)
	(machine-instance)))
(def-scalar-variable [sysLocation] (agent)
    (labels ((console-location (host)
	       (or (zl:send host :get :console-location)
		   (and (setq host 
			  (cadr (assoc :embedded-in
				       (zl:send host :get :user-property))))
			(console-location host)))))
      (format nil "~{~A~^-~}"
	      (console-location net:*local-host*))))
(def-scalar-variable [sysServices] (agent)
    #.(logior (expt 2 (1- 4))	;end-to-end
	      (expt 2 (1- 7))))	;applications

(def-scalar-variable [ifNumber] (agent)
	 (length neti:*interfaces*))

(def-listy-mib-table [ifTable] (:list neti:*interfaces*)
  (:entry [ifEntry])
  (:slots
   ([ifIndex] (interface ignore subids) (first subids))
   ([ifDescr] (interface) (neti:network-interface-name interface))
   ([ifType]
    (interface)
    (typecase interface
      (neti:ethernet-interface 6)	     ;ethernet-csmacd
      (t 1)))				     ;other
   ([ifMtu] (interface) (zl:send interface :max-packet-size))
   ([ifSpeed] 
    (interface)
    (make-gauge32
     (typecase interface
       (neti:ethernet-interface 10000000)
       (t 1))))
   ([ifPhysAddress] 
    (interface)
    (slot-value interface 'neti:local-hardware-address))
   ([ifAdminStatus]
    (interface)
    (if (slot-value interface 'neti:enabled)
	1				     ;up
      2))
   ([ifOperStatus]
    (interface)
    (if (slot-value interface 'neti:enabled)
	1				     ;up
      2))				     ;down
   ([ifLastChange] (interface) (make-timeticks 0))
   ([ifInOctets] (interface) (make-counter32 0))
   ([ifInUcastPkts]
    (interface)
    (make-counter32 (slot-value interface 'neti:receive-count)))
   ([ifInNUcastPkts] (interface) (make-counter32 0))
   ([ifInDiscards] (interface) (make-counter32 (interface-in-errors interface)))
   ([ifInErrors] (interface) (make-counter32 (interface-in-errors interface)))
   ([ifInUnknownProtos] (interface) (make-counter32 0))
   ([ifOutOctets] (interface) (make-counter32 0))
   ([ifOutUcastPkts] 
    (interface)
    (make-counter32 (slot-value interface 'neti:transmit-count)))
   ([ifOutNUcastPkts] (interface) (make-counter32 0))
   ([ifOutDiscards] (interface) (make-counter32 0))
   ([ifOutErrors] (interface) (make-counter32 (interface-out-errors interface)))
   ([ifOutQLen] (interface) (make-gauge32 0))))

(defmethod interface-in-errors ((interface neti:network-interface)) 0)
(defmethod interface-in-discards ((interface neti:network-interface)) 0)
(defmethod interface-out-errors ((interface neti:network-interface)) 0)

#.(and (find-class 'neti:82586-ethernet-interface nil)
'(progn

(defmethod interface-in-errors ((interface neti:82586-ethernet-interface))
  (reduce #'+ (mapcar #'(lambda (meter)
			  (neti:82586-meter-value interface meter))
		      '(:crc-errors :alignment-errors :overruns
			:short-packets))))

(defmethod interface-in-discards ((interface neti:82586-ethernet-interface))
  (reduce #'+ (mapcar #'(lambda (meter)
			  (neti:82586-meter-value interface meter))
		      '(:no-resources))))

(defmethod interface-out-errors ((interface neti:82586-ethernet-interface))
  (reduce #'+ (mapcar #'(lambda (meter)
			  (neti:82586-meter-value interface meter))
		      '(:transmit-aborts))))
))

#.(and (find-class 'cli::nbs-ethernet-interface nil)
'(progn

(defmethod interface-in-errors ((interface cli::nbs-ethernet-interface)) 0)

(defmethod interface-in-discards ((interface cli::nbs-ethernet-interface)) 0)

(defmethod interface-out-errors ((interface cli::nbs-ethernet-interface)) 0)
))

(def-scalar-variable [ifSpecific] (agent) (make-object-id '(0 0)))

;;;; the `at' group

(zl:defmethod (:at-entries neti:network-interface) () '())
(zl:defmethod (:at-entries neti:address-resolution-interface-mixin) ()
  (let ((ats '())
	(networks-seen '()))
    (dolist (protocol-table-entry 
		neti:protocol-table 
	      (remove-duplicates ats :key #'car :test #'equal))
      (let ((network (neti:protocol-table-entry-network protocol-table-entry)))
	(and (not (member network networks-seen :test #'eq))
	     ;; in reality, we can only put IP-to-PhysAddr mappings
	     ;; in the atTable.
	     (typep network 'tcp:internet-network)
	     (push network networks-seen)
	     (neti:protocol-table-entry-hash-table protocol-table-entry)
	     (maphash #'(lambda (key value)
			  (push (cons (zl:send
				       (neti:protocol-table-entry-network 
					protocol-table-entry)
				       :address-to-list
				       key)
				      value) ats))
		      (neti:protocol-table-entry-hash-table
		       protocol-table-entry)))))))

(zl:defmethod (:address-to-list tcp:internet-network) (address)
  (ip-addr-numeric->list address))

(zl:defmethod (:address-to-list neti:slap-network) (address)
  (coerce address 'list))

(zl:defmethod (:address-to-list chaos:chaos-network) (address)
  (list (logand #xff (ash address -8))
	(logand #xff address)))

(zl:defmethod (:address-to-list si:fband-network) (address)
  (list (logand #xff (ash address -8))
	(logand #xff address)))
  
(def-listy-mib-table [atTable]
    (:list
     (let ((if-count 0))
       (mapcan #'(lambda (interface)
		   (incf if-count)
		   (mapcar #'(lambda (x) (cons if-count x))
			   (zl:send interface :at-entries)))
	       neti:*interfaces*)))
  (:index #'(lambda (at)
	      (list* (car at) (car at)                ;???
		     (cadr at))))
  (:sort t)
  (:entry [atEntry])
  (:slots
   ([atIfIndex] (at) (car at))
   ([atPhysAddress] (at) (cddr at))
   ([atNetAddress] (at) (make-ip-address-from-list (cadr at)))))

;;;; the `ip' group

(defun the-ip-protocol ()
  (tcp::internet-network-ip-protocol
   (neti:find-network-named :internet)))

(def-scalar-variable [ipForwarding] (agent) 2);not-fowarding
(def-scalar-variable [ipDefaultTTL] (agent)
    64)				;hard-coded in method 
				; (:transmit-packet ip-protocol)
(def-scalar-variable [ipInReceives] (agent)
    (make-counter32 (zl:send (the-ip-protocol) :pkts-in)))
(def-scalar-variable [ipInHdrErrors] (agent)
    (make-counter32 (- (zl:send (the-ip-protocol) :pkts-in)
		       (zl:send (the-ip-protocol) :pkts-received))))
(def-scalar-variable [ipInAddrErrors] (agent)
    (make-counter32 0))
(def-scalar-variable [ipForwDatagrams] (agent)
    (make-counter32 (zl:send (the-ip-protocol) :pkts-routed)))
(def-scalar-variable [ipInUnknownProtos] (agent)
    (make-counter32 0))		;<#>
(def-scalar-variable [ipInDiscards] (agent)
    (make-counter32 0))
(def-scalar-variable [ipInDelivers] (agent)
    (make-counter32 (zl:send (the-ip-protocol) :pkts-received)))
(def-scalar-variable [ipOutRequests] (agent)
    (make-counter32 (zl:send (the-ip-protocol) :pkts-transmitted)))
(def-scalar-variable [ipOutDiscards] (agent)
    (make-counter32 0))
(def-scalar-variable [ipOutNoRoutes] (agent)
    (make-counter32 (- (zl:send (the-ip-protocol) :pkts-transmitted)
		       (zl:send (the-ip-protocol) :pkts-out))))

;; The value of 64 is somewhat artificial.  In reality, the TTL field
;; of an IP packet seems to define how long the packet is kept in the
;; reassembly buffer.
;;
(def-scalar-variable [ipReasmTimeout] (agent)
    64)

;; Unfortunately this is not counted in the standard IP code.
;;
(def-scalar-variable [ipReasmReqds] (agent)
    (make-counter32 0))
(def-scalar-variable [ipReasmOKs] (agent)
    (make-counter32 0))
(def-scalar-variable [ipReasmFails] (agent)
    (make-counter32 0))
(def-scalar-variable [ipFragOKs] (agent)
    (make-counter32 0))
(def-scalar-variable [ipFragFails] (agent)
    (make-counter32 0))
(def-scalar-variable [ipFragCreates] (agent)
    (make-counter32 0))

(def-listy-mib-table [ipAddrTable]
    (:list (get-ip-address-table))
  (:entry [ipAddrEntry])
  (:slots
   ([ipAdEntAddr])
   ([ipAdEntIfIndex])
   ([ipAdEntNetMask])
   ([ipAdEntBcastAddr])
   ([ipAdEntReasmMaxSize])))

#|
((21)				;ipRouteTable
 (let ((index (pop subids)))
   (let ((entry (ip-routing-table-entry index))) ;ipRouteEntry
     (case (pop subids)
       ((1))			;ipRouteDest
       ((2))			;ipRouteIfIndex
       ((3))			;ipRouteMetric1
       ((4))			;ipRouteMetric2
       ((5))			;ipRouteMetric3
       ((6))			;ipRouteMetric5
       ((7))			;ipRouteNextHop
       ((8))			;ipRouteType
       ((9))			;ipRouteProto
       ((10))			;ipRouteAge
       ((11))			;ipRouteMask
       ((12))			;ipRouteMetric5
       ((13))))))		;ipRouteInfo
|#

#|
((22)				;ipNetToMediaTable
 (let ((index (pop subids)))
   (let ((entry (ip-net-to-media-table-entry index))) ;ipNetToMediaEntry
     (case (pop subids)
       ((1))			;ipNetToMediaIfIndex
       ((2))			;ipNetToMediaPhysAddress
       ((3))			;ipNetToMediaNetAddress
       ((4))))))		;ipNetToMediaType
|#
(def-scalar-variable [ipRoutingDiscards] (agent))

;;;; the `icmp' group

(def-scalar-variable [icmpInMsgs] (agent))
(def-scalar-variable [icmpInErrors] (agent))
(def-scalar-variable [icmpInDestUnreachs] (agent))
(def-scalar-variable [icmpInTimeExcds] (agent))
(def-scalar-variable [icmpInParmProbs] (agent))
(def-scalar-variable [icmpInSrcQuenchs] (agent))
(def-scalar-variable [icmpInRedirects] (agent))
(def-scalar-variable [icmpInEchos] (agent))
(def-scalar-variable [icmpInEchoReps] (agent))
(def-scalar-variable [icmpInTimestamps] (agent))
(def-scalar-variable [icmpInTimestampReps] (agent))
(def-scalar-variable [icmpInAddrMasks] (agent))
(def-scalar-variable [icmpInAddrMaskReps] (agent))
(def-scalar-variable [icmpOutMsgs] (agent))
(def-scalar-variable [icmpOutErrors] (agent))
(def-scalar-variable [icmpOutDestUnreachs] (agent))
(def-scalar-variable [icmpOutTimeExcds] (agent))
(def-scalar-variable [icmpOutParmProbs] (agent))
(def-scalar-variable [icmpOutSrcQuenchs] (agent))
(def-scalar-variable [icmpOutRedirects] (agent))
(def-scalar-variable [icmpOutEchos] (agent))
(def-scalar-variable [icmpOutEchoReps] (agent))
(def-scalar-variable [icmpOutTimestamps] (agent))
(def-scalar-variable [icmpOutTimestampReps] (agent))
(def-scalar-variable [icmpOutAddrMasks] (agent))
(def-scalar-variable [icmpOutAddrMaskReps] (agent))

;;;; the `tcp' group

(def-scalar-variable [tcpRtoAlgorithm] (agent))
(def-scalar-variable [tcpRtoMin] (agent))
(def-scalar-variable [tcpRtoMax] (agent))
(def-scalar-variable [tcpMaxConn] (agent))
(def-scalar-variable [tcpActiveOpens] (agent))
(def-scalar-variable [tcpPassiveOpens] (agent))
(def-scalar-variable [tcpAttemptFails] (agent))
(def-scalar-variable [tcpEstabResets] (agent))
(def-scalar-variable [tcpCurrEstab] (agent))
(def-scalar-variable [tcpInSegs] (agent))
(def-scalar-variable [tcpOutSegs] (agent))
(def-scalar-variable [tcpRetransSegs] (agent))

(defmethod tcp-conn-table-entry-index (conn)
  (append (ip-addr-numeric->list (tcp::tcb-local-address conn))
	  (list (tcp::tcb-local-port conn))
	  (ip-addr-numeric->list (tcp::tcb-foreign-address conn))
	  (list (tcp::tcb-foreign-port conn))))

(def-listy-mib-table [tcpConnTable]
    (:list tcp::*tcb-list*)
  (:entry [tcpConnEntry])
  (:index #'tcp-conn-table-entry-index)
  (:sort t)
  (:slots
   ([tcpConnState] 
    (conn)
    (case (tcp::tcb-state conn)
      ((:closed) 1)
      ((:listen) 2)
      ((:syn-sent) 3)
      ((:syn-received) 4)
      ((:established) 5)
      ((:fin-wait-1) 6)
      ((:fin-wait-2) 7)
      ((:close-wait) 8)
      ((:last-ack) 9)
      ((:closing) 10)
      ((:time-wait) 11)))
   ([tcpConnLocalAddress] 
    (conn)
    (make-ip-address-from-number (tcp::tcb-local-address conn)))
   ([tcpConnLocalPort]
    (conn)
    (tcp::tcb-local-port conn))
   ([tcpConnRemAddress]
    (conn)
    (make-ip-address-from-number (tcp::tcb-foreign-address conn)))
   ([tcpConnRemPort]
    (conn)
    (tcp::tcb-foreign-port conn))))

(def-scalar-variable [tcpInErrs] (agent))
(def-scalar-variable [tcpOutRsts] (agent))

;;;; the `udp' group

(defun the-udp-protocol ()
  (tcp::internal-ip-protocol-instance
   (find-if #'(lambda (x)
		(and x 
		     (typep (tcp::internal-ip-protocol-instance x)
			    'tcp::udp-protocol)))
	    (zl:send (the-ip-protocol) :protocol-array))))

(def-scalar-variable [udpInDatagrams] (agent)
    (make-counter32 (zl:send (the-udp-protocol) :pkts-in)))
(def-scalar-variable [udpNoPorts] (agent)
    (make-counter32 (zl:send (the-udp-protocol) :bad-port-errors)))
(def-scalar-variable [udpInErrors] (agent)
    (make-counter32 (+ (zl:send (the-udp-protocol) :bad-format-errors)
		       (zl:send (the-udp-protocol) :checksum-errors))))
(def-scalar-variable [udpOutDatagrams] (agent)
    (make-counter32 (zl:send (the-udp-protocol) :pkts-out)))

#| This would be an implementation of the udpConnTable --
   unfortunately there is no such thing.

(defun udp-conn-table-index (conn)
  (append (ip-addr-numeric->list (zl:send conn :local-address))
	  (list (zl:send conn :local-port))))

(def-listy-mib-table [udpConnTable]
    (:list (zl:send (the-udp-protocol) :conn-list))
  (:entry [udpConnEntry])
  (:sort t)
  (:index #'udp-conn-table-index)
  (:slots
   ([udpLocalAddress] 
    (conn)
    (make-ip-address-from-number (zl:send conn :local-address)))
   ([udpLocalPort] (conn) (zl:send conn :local-port))))

|#

(defun udp-table-index (entry) (append '(0 0 0 0) (list (car entry))))

(def-listy-mib-table [udpTable]
    (:list tcp::*udp-protocol-alist*)
  (:entry [udpEntry])
  (:sort t)
  (:index #'udp-table-index)
  (:compare-function #'(lambda (a b) (< (car a) (car b))))
  (:slots
   ([udpLocalAddress] 
    (entry)
    (make-ip-address-from-octet-string
     (make-array '(4)
		 :element-type '(unsigned-byte 8)
		 :initial-contents '(0 0 0 0))))
   ([udpLocalPort] (entry) (car entry))))

;;;; the `egp' group
;;;
;;; This code is untested because we don't have any LISPMs that
;;; really perform EGP.

(defun the-egp-protocol ()
  (tcp::internal-ip-protocol-instance
   (find-if #'(lambda (x)
		(and x 
		     (typep (tcp::internal-ip-protocol-instance x)
			    'tcp::egp-protocol)))
	    (zl:send (the-ip-protocol) :protocol-array))))

(def-scalar-variable [egpInMsgs] (agent)
    (zl:send (the-egp-protocol) :pkts-in))
(def-scalar-variable [egpInErrors] (agent)
    (+ (zl:send (the-egp-protocol) :format-errors)
       (zl:send (the-egp-protocol) :checksum-errors)))
(def-scalar-variable [egpOutMsgs] (agent)
    (zl:send (the-egp-protocol) :pkts-out))
(def-scalar-variable [egpOutErrors] (agent)
    (zl:send (the-egp-protocol) :errors-out))

(def-listy-mib-table [egpNeighTable]
    (:list (let ((result '()))
	     (maphash #'(lambda (key value)
			  (declare (ignore key))
			  (push value result))
		      (zl:send (the-egp-protocol) :neighbor-hash-table))
	     result))
  (:index #'(lambda (neighbor) 
	      (ip-addr-numeric->list (tcp::egp-n-address neighbor))))
  (:sort t)
  (:compare-function
   #'(lambda (a b)
       (< (logand #xffff (tcp::egp-n-address a))
	  (logand #xffff (tcp::egp-n-address b)))))
  (:entry [egpNeighEntry])
  (:slots
   ([egpNeighState]
    (neighbor)
    (case (tcp::egp-n-state neighbor)
      ((:idle) 1)
      ((:acquire) 2)
      ((:down) 3)
      ((:up) 4)
      ((:cease) 5)		;Genera doesn't seem to know this state.
      ))
   ([egpNeighAddr]
    (neighbor)
    (make-ip-address-from-number (tcp::egp-n-address neighbor)))
   ([egpNeighAs]
    (neighbor)
    (tcp::egp-n-system neighbor))
   ([egpNeighInMsgs]
    (neighbor)
    (make-counter32 0))		;not recorded
   ([egpNeighInErrs]
    (neighbor)
    (make-counter32 0))		;not recorded
   ([egpNeighOutMsgs]
    (neighbor)
    (make-counter32 0))		;not recorded
   ([egpNeighOutErrs]
    (neighbor)
    (make-counter32 0))		;not recorded
   ([egpNeighInErrMsgs]
    (neighbor)
    (make-counter32 0))		;not recorded
   ([egpNeighOutErrMsgs]
    (neighbor)
    (make-counter32 0))		;not recorded
   ([egpNeighStateUps]
    (neighbor)
    (make-counter32 0))		;not recorded
   ([egpNeighStateDowns]
    (neighbor)
    (make-counter32 0))		;not recorded
   ([egpNeighIntervalHello]
    (neighbor)
    (* (tcp::egp-n-hello-interval neighbor) 100))
   ([egpNeighIntervalPoll]
    (neighbor)
    (* (tcp::egp-n-poll-interval neighbor) 100))
   ([egpNeighMode]
    (neighbor))
   ([egpNeighEventTrigger]
    (neighbor))))

(def-scalar-variable [egpAs] (agent))

;;;; the `cmot' group

;;;; the `transmission' group

;;;; the `snmp' group

(def-scalar-variable [snmpInPkts] (agent)
    (make-counter32 (sas-in-pkts *the-snmp-agent*)))
(def-scalar-variable [snmpOutPkts] (agent)
    (make-counter32 (sas-out-pkts *the-snmp-agent*)))
(def-scalar-variable [snmpInBadVersions] (agent)
    (make-counter32 (sas-in-bad-versions *the-snmp-agent*)))
(def-scalar-variable [snmpInBadCommunityNames] (agent)
    (make-counter32 (sas-in-bad-community-names *the-snmp-agent*)))
(def-scalar-variable [snmpInBadCommunityUses] (agent)
    (make-counter32 (sas-in-bad-community-uses *the-snmp-agent*)))
(def-scalar-variable [snmpInASNParseErrs] (agent)
    (make-counter32 (sas-in-asn-parse-errs *the-snmp-agent*)))
(def-scalar-variable [snmpInTooBigs] (agent)
    (make-counter32 (sas-in-too-bigs *the-snmp-agent*)))
(def-scalar-variable [snmpInNoSuchNames] (agent)
    (make-counter32 (sas-in-no-such-names *the-snmp-agent*)))
(def-scalar-variable [snmpInBadValues] (agent)
    (make-counter32 (sas-in-bad-values *the-snmp-agent*)))
(def-scalar-variable [snmpInReadOnlys] (agent)
    (make-counter32 (sas-in-read-onlys *the-snmp-agent*)))
(def-scalar-variable [snmpInGenErrs] (agent)
    (make-counter32 (sas-in-gen-errs *the-snmp-agent*)))
(def-scalar-variable [snmpInTotalReqVars] (agent)
    (make-counter32 (sas-in-total-req-vars *the-snmp-agent*)))
(def-scalar-variable [snmpInTotalSetVars] (agent)
    (make-counter32 (sas-in-total-set-vars *the-snmp-agent*)))
(def-scalar-variable [snmpInGetRequests] (agent)
    (make-counter32 (sas-in-get-requests *the-snmp-agent*)))
(def-scalar-variable [snmpInGetNexts] (agent)
    (make-counter32 (sas-in-get-nexts *the-snmp-agent*)))
(def-scalar-variable [snmpInSetRequests] (agent)
    (make-counter32 (sas-in-set-requests *the-snmp-agent*)))
(def-scalar-variable [snmpInGetResponses] (agent)
    (make-counter32 (sas-in-get-responses *the-snmp-agent*)))
(def-scalar-variable [snmpInTraps] (agent)
    (make-counter32 (sas-in-traps *the-snmp-agent*)))
(def-scalar-variable [snmpOutTooBigs] (agent)
    (make-counter32 (sas-out-too-bigs *the-snmp-agent*)))
(def-scalar-variable [snmpOutNoSuchNames] (agent)
    (make-counter32 (sas-out-no-such-names *the-snmp-agent*)))
(def-scalar-variable [snmpOutBadValues] (agent)
    (make-counter32 (sas-out-bad-values *the-snmp-agent*)))
(def-scalar-variable [snmpOutGenErrs] (agent)
    (make-counter32 (sas-out-gen-errs *the-snmp-agent*)))
(def-scalar-variable [snmpOutGetRequests] (agent)
    (make-counter32 (sas-out-get-requests *the-snmp-agent*)))
(def-scalar-variable [snmpOutGetNexts] (agent)
    (make-counter32 (sas-out-get-nexts *the-snmp-agent*)))
(def-scalar-variable [snmpOutSetRequests] (agent)
    (make-counter32 (sas-out-set-requests *the-snmp-agent*)))
(def-scalar-variable [snmpOutGetResponses] (agent)
    (make-counter32 (sas-out-get-responses *the-snmp-agent*)))
(def-scalar-variable [snmpOutTraps] (agent)
    (make-counter32 (sas-out-traps *the-snmp-agent*)))
(def-scalar-variable [snmpEnableAuthenTraps] (agent)
    (if (sas-enable-authen-traps *the-snmp-agent*) 1 2))

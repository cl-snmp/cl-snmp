;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  view.lisp
;;; Description:  SNMP Agent - View-Based Access Control
;;; Author:	  Simon Leinen  <simon@switch.ch>
;;; Date Created: 28-Mar-1999
;;; RCS $Header: /home/leinen/cl/sysman/agent/generic/RCS/view.lisp,v 1.1 1999/03/31 14:11:08 simon Exp leinen $
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :snmp)

(defclass mib-view ()
  ((subtrees :initform '((() nil
			     (((1 3 6 1 2 1 1) t))
			     (((1 3 6 1 4 1 2946 1 1 1) t))))
	     :accessor mib-view-subtrees
	     :type list)))

(defmethod oid-readable-in-view-p (oid (view mib-view))
  (oid-readable-in-view-p-1 (object-id-subids oid) view))

(defmethod oid-readable-in-view-p-1 (subids (view mib-view))
  (oid-readable-in-view-p-2 subids (mib-view-subtrees view) nil))

(defmethod oid-readable-in-view-p-2 (subids subtrees default)
  (cond ((eq subtrees 't) t)
	((endp subtrees) default)
	((oid-matches-subtree-p subids (first (first subtrees)))
	 (oid-readable-in-view-p-2
	  (subseq subids (length (first (first subtrees))))
	  (third (first subtrees))
	  (second (first subtrees))))
	(t (oid-readable-in-view-p-2 subids (rest subtrees) default))))

(defun oid-matches-subtree-p (subids1 subids2)
  (or (endp subids2)
      (and (not (endp subids1))
	   (= (first subids1) (first subids2))
	   (oid-matches-subtree-p (rest subids1) (rest subids2)))))

;;; -*- Package: SNMP; mode: Common-Lisp -*-
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  generic-mib-2.lisp
;;; Description:  Generic MIB 2 Instrumentation
;;; Author:	  Simon Leinen (simon@switch.ch)
;;; Date Created: 31-Jan-1999
;;; RCS $Header: /home/leinen/cl/sysman/agent/generic/RCS/mib-2.lisp,v 1.3 2002/08/11 21:50:32 leinen Exp leinen $
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :snmp)

(eval-when (:compile-toplevel :execute)
  (setq *readtable* *snmp-readtable*))

(def-scalar-variable [sysDescr] (agent)
    (machine-type))
(def-scalar-variable [sysObjectID] (agent)
    '#.(make-object-id '(1 3 6 1 4 1 4 1 2 1 6 9 3 5 2 7 7)))
(def-scalar-variable [sysUpTime] (agent)
    (make-timeticks 
     (truncate (* #.(/ 100 internal-time-units-per-second)
		  (- (get-internal-real-time)
		     (sas-start-up-time agent))))))

(def-scalar-variable [sysServices] (agent)
    #.(logior (expt 2 (1- 4))	;end-to-end
	      (expt 2 (1- 7))))	;applications

;;;; the `snmp' group

(def-scalar-variable [snmpInPkts] (agent)
    (make-counter32 (sas-in-pkts agent)))
(def-scalar-variable [snmpOutPkts] (agent)
    (make-counter32 (sas-out-pkts agent)))
(def-scalar-variable [snmpInBadVersions] (agent)
    (make-counter32 (sas-in-bad-versions agent)))
(def-scalar-variable [snmpInBadCommunityNames] (agent)
    (make-counter32 (sas-in-bad-community-names agent)))
(def-scalar-variable [snmpInBadCommunityUses] (agent)
    (make-counter32 (sas-in-bad-community-uses agent)))
(def-scalar-variable [snmpInASNParseErrs] (agent)
    (make-counter32 (sas-in-asn-parse-errs agent)))
(def-scalar-variable [snmpInTooBigs] (agent)
    (make-counter32 (sas-in-too-bigs agent)))
(def-scalar-variable [snmpInNoSuchNames] (agent)
    (make-counter32 (sas-in-no-such-names agent)))
(def-scalar-variable [snmpInBadValues] (agent)
    (make-counter32 (sas-in-bad-values agent)))
(def-scalar-variable [snmpInReadOnlys] (agent)
    (make-counter32 (sas-in-read-onlys agent)))
(def-scalar-variable [snmpInGenErrs] (agent)
    (make-counter32 (sas-in-gen-errs agent)))
(def-scalar-variable [snmpInTotalReqVars] (agent)
    (make-counter32 (sas-in-total-req-vars agent)))
(def-scalar-variable [snmpInTotalSetVars] (agent)
    (make-counter32 (sas-in-total-set-vars agent)))
(def-scalar-variable [snmpInGetRequests] (agent)
    (make-counter32 (sas-in-get-requests agent)))
(def-scalar-variable [snmpInGetNexts] (agent)
  (make-counter32 (sas-in-get-nexts agent)))
(def-scalar-variable [snmpInSetRequests] (agent)
    (make-counter32 (sas-in-set-requests agent)))
(def-scalar-variable [snmpInGetResponses] (agent)
    (make-counter32 (sas-in-get-responses agent)))
(def-scalar-variable [snmpInTraps] (agent)
    (make-counter32 (sas-in-traps agent)))
(def-scalar-variable [snmpOutTooBigs] (agent)
    (make-counter32 (sas-out-too-bigs agent)))
(def-scalar-variable [snmpOutNoSuchNames] (agent)
    (make-counter32 (sas-out-no-such-names agent)))
(def-scalar-variable [snmpOutBadValues] (agent)
    (make-counter32 (sas-out-bad-values agent)))
(def-scalar-variable [snmpOutGenErrs] (agent)
    (make-counter32 (sas-out-gen-errs agent)))
(def-scalar-variable [snmpOutGetRequests] (agent)
    (make-counter32 (sas-out-get-requests agent)))
(def-scalar-variable [snmpOutGetNexts] (agent)
    (make-counter32 (sas-out-get-nexts agent)))
(def-scalar-variable [snmpOutSetRequests] (agent)
    (make-counter32 (sas-out-set-requests agent)))
(def-scalar-variable [snmpOutGetResponses] (agent)
    (make-counter32 (sas-out-get-responses agent)))
(def-scalar-variable [snmpOutTraps] (agent)
    (make-counter32 (sas-out-traps agent)))
(def-scalar-variable [snmpEnableAuthenTraps] (agent)
    (if (sas-enable-authen-traps agent) 1 2))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  mib-instr.lisp
;;; Description:  Instrumentation for MIBs
;;; Author:	  Simon Leinen (simon@lia.di.epfl.ch)
;;; Date Created:  2-Nov-93
;;; RCS $Header: /home/leinen/cl/sysman/agent/generic/RCS/mib-instr.lisp,v 1.12 2002/08/11 21:50:35 leinen Exp leinen $  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :snmp)

(defclass mib-variable () 
  ((name :reader mib-variable-name
	 :initarg :name
	 :type string)))

(defun mib-variable-p (x) (typep x 'mib-variable))

(defmethod print-object ((var mib-variable) stream)
  (if (slot-boundp var 'name)
      (print-unreadable-object (var stream :type t)
	(princ (mib-variable-name var) stream))
    (print-unreadable-object (var stream :type t :identity t))))

(defclass scalar-mib-variable (mib-variable)
  ())

(defclass tabular-mib-variable (mib-variable)
  ((entry-size :initarg :entry-size
	       :reader tabular-mib-variable-entry-size)))

(defclass readable-scalar-mib-variable (scalar-mib-variable)
  ((get-function
    :type (function () t)
    :reader mv-get-function
    :initarg :get-function)))

(defmethod get-next-variable ((v scalar-mib-variable) subids agent)
  (and (endp subids) (values '(0) (get-variable v '(0) agent))))

(defgeneric get-next-table-entry (v subids agent)
  (declare (type tabular-mib-variable v) (list subids)))

(defclass tcaching-tabular-mib-variable (tabular-mib-variable)
  ((tcache
    :initform nil
    :accessor mib-tabular-variable-tcache)))

(defun add-entry-to-cache (cache entry restoid subids)
  (declare (ignore cache))
  (list subids restoid entry))

(defclass simple-tcaching-tabular-mib-variable (tcaching-tabular-mib-variable)
  ())

(defun get-simple-tcache-entry (c index)
  (labels ((get-simple-tcache-entry-1 (i1 value i2)
	     (if (= i1 i2)
		 (values value i1)
		 (and (not (endp value))
		      (not (> i1 i2))
		      (get-simple-tcache-entry-1 (1+ i1) (rest value) i2)))))
    (and (not (null c))
	 (not (zerop index))
	 (get-simple-tcache-entry-1 (car c) (cdr c) index))))

(defun add-entry-to-simple-tcache (c tail index)
  (declare (ignore c))
  (cons index tail))

(defun add-entry-to-tcache (c tail) (declare (ignore c)) tail)

(defun get-tcache-entry (table c subids)
  (labels ((get-tcache-entry-1 (value subids2)
	     (and (not (endp value))
		  (let ((subids1 (table-entry-index table (first value))))
		    (if (oid-list->= subids1 subids2)
			(values value subids1)
		      (and (not (oid-list-< subids2 subids1))
			   (get-tcache-entry-1 (rest value) subids2)))))))
    (and (not (null c))
	 (not (null subids))
	 (get-tcache-entry-1 c subids))))

(defmethod get-next-table-entry ((table tcaching-tabular-mib-variable) subids agent)
  (let ((tail (get-tcache-entry table
				    (mib-tabular-variable-tcache table) 
				    subids)))
    (if tail
	(let ((value (if (oid-list-< subids (table-entry-index table (first tail)))
			 (first tail)
		       (second tail))))
	  (values value (and value (table-entry-index table value))))
      (progn
	(setq tail (compute-next-table-tail table subids agent))
	(when tail
	  (setf (mib-tabular-variable-tcache table)
	    (add-entry-to-tcache
	     (mib-tabular-variable-tcache table)
	     tail)))
	(and tail
	     (let ((value (if (oid-list-<
			       subids
			       (table-entry-index table (first tail)))
			      (first tail)
			    (second tail))))
	       (values value (and value (table-entry-index table value)))))))))

(defmethod get-next-table-entry ((v simple-tcaching-tabular-mib-variable) subids agent)
  (multiple-value-bind (tail index)
      (get-simple-tcache-entry (mib-tabular-variable-tcache v) 
			    (or (first subids) 0))
    (if tail
	(values (if subids (second tail) (first tail)) 
		(list (if subids (1+ index) index)))
      (progn
	(multiple-value-setq (tail index)
	  (compute-next-table-tail v subids agent))
	(when tail
	  (setf (mib-tabular-variable-tcache v)
	    (add-entry-to-simple-tcache
	     (mib-tabular-variable-tcache v)
	     tail index)))
	(and tail (values (first tail) (list index)))))))

(defclass listy-mib-table ()
  ((get-list-fn :type (function (table t) list)
		:initarg :get-list-fn
		:reader listy-mib-table-get-list-fn)
   (get-component-fns
    :type (simple-array (function (listy-mib-table t index subids) t) (*))
    :initarg :get-component-fns
    :reader listy-mib-table-get-component-fns)
   (test-fn :type (function (table t) list)
	    :initarg :test-fn
	    :initform #'(lambda (table entry)
			  (declare (ignore table entry))
			  t)
	    :reader listy-mib-table-test-fn)))

(defmethod get-next-variable ((v listy-mib-table) subids agent)
  (let ((get-component-fns (listy-mib-table-get-component-fns v)))
    (do ((subindex (or (pop subids) 1) (1+ subindex)))
	((> subindex (tabular-mib-variable-entry-size v)) nil)
      (when (aref get-component-fns subindex)
	(return
	  (multiple-value-bind (entry restoid)
	      (get-next-table-entry v subids agent)
	    (if entry
		(multiple-value-bind (subcomponent)
		    (get-entry-subcomponent v entry subindex restoid)
		  (and subcomponent
		       (values
			(cons subindex restoid)
			subcomponent)))
	      (get-next-variable v (list (1+ subindex)) agent))))))))

(defclass simple-listy-mib-table (listy-mib-table simple-tcaching-tabular-mib-variable)
  ())

(defclass complex-listy-mib-table (listy-mib-table tcaching-tabular-mib-variable)
  ((entry-index-function
    :type (function (listy-mib-table t) list)
    :initarg :entry-index-function
    :reader listy-mib-table-entry-index-function)))

(defmethod get-variable ((v listy-mib-table) subids agent)
  (let ((column (first subids))
	(index (rest subids)))
    (let ((entry (find-table-entry v index agent)))
      (and entry
	   (get-entry-subcomponent v entry column '())))))

(defmethod find-table-entry ((tt simple-listy-mib-table) index agent)
  (nth (1- (first index)) (funcall (listy-mib-table-get-list-fn tt) tt agent)))

(defmethod table-entry-index ((tt listy-mib-table) entry)
  (funcall (listy-mib-table-entry-index-function tt) entry))

(defmethod compute-next-table-tail ((tt complex-listy-mib-table) subids agent)
  (loop for tail on (funcall (listy-mib-table-get-list-fn tt) tt agent)
      do (when (and (funcall (listy-mib-table-test-fn tt) tt (first tail))
		    (oid-list->= (table-entry-index tt (first tail)) subids))
	   (return-from compute-next-table-tail tail)))
  '())

(defmethod find-table-entry ((tt complex-listy-mib-table) index agent)
  (let ((table-entries (funcall (listy-mib-table-get-list-fn tt) tt agent)))
    (find index table-entries
	  :test #'equal
	  :key #'(lambda (entry)
		   (funcall (listy-mib-table-entry-index-function tt)
			    entry)))))

(defmethod compute-next-table-tail ((tt simple-listy-mib-table) subids agent)
  (let ((list (funcall (listy-mib-table-get-list-fn tt) tt agent))
	(index (if subids (first subids) 0)))
    (let ((value (nthcdr index list)))
      (and value (values value (1+ index))))))

(defmethod get-entry-subcomponent ((v listy-mib-table) entry subindex restoid)
  (let ((get-component-fn (aref (listy-mib-table-get-component-fns v) subindex)))
    (and get-component-fn
	 (funcall get-component-fn entry restoid))))      

(defmethod get-variable ((v readable-scalar-mib-variable) subids agent)
  (and (equal subids '(0))
       (funcall (mv-get-function v) agent)))

(defun mib-get-value (mib subids agent view)
  (multiple-value-bind (variable subids-rest)
      (id-tree-get (mib-id-tree mib) subids)
    (and variable
	 (get-variable variable subids-rest agent))))

(defun get-next-internal (tree subids prefix past-p agent view)
  (let ((variable (find-if #'mib-variable-p (id-tree-values tree))))
    (if variable
	(multiple-value-bind (new-subids value)
	    (get-next-variable variable subids agent)
	  (and value (values (revappend prefix new-subids) value)))
      (let ((subtree (id-tree-nth-subtree tree (first subids))))
	(if subtree
	    (multiple-value-bind (next-subids value)
		(get-next-internal 
		 subtree
		 (rest subids)
		 (cons (first subids) prefix)
		 past-p agent view)
	      (if next-subids
		  (values next-subids value)
		(dolist (next-subtree
			    (id-tree-next-subtrees tree (if (endp subids) 0 (first subids)))
			  nil)
		  (multiple-value-bind (subids value)
		      (get-next-internal
		       (cdr next-subtree)
		       '()
		       (cons (first next-subtree) prefix)
		       t agent view)
		    (when subids (return (values subids value)))))))
	  (dolist (next-subtree
		      (id-tree-next-subtrees tree (if (endp subids) 0 (first subids)))
		    nil)
	    (multiple-value-bind (subids value)
		(get-next-internal
		 (cdr next-subtree)
		 '()
		 (cons (first next-subtree) prefix)
		 t agent view)
	      (when subids (return (values subids value))))))))))

(defun mib-get-next-value (mib subids agent view)
  (get-next-internal (mib-id-tree mib) subids '() nil agent view))

;;;; Macros for defining how MIB variables are computed

(defmacro def-scalar-variable (oid (arg) &body get-body)
  (let ((subids (object-id-subids oid))
	(getter-name (intern (format nil "SNMP get function for ~A"
				     (princ-to-string oid))
			     (find-package :snmp))))
    `(progn
       (defun ,getter-name (,arg) (declare (ignorable ,arg)) ,@get-body)
       (add-node-with-values-to-id-tree
	',subids
	(list (make-instance 'readable-scalar-mib-variable
		:get-function #',getter-name
		:name ,(princ-to-string oid)))
	(mib-id-tree *mib*)
	#'(lambda (x) (typep x 'mib-variable))))))

(defmacro def-listy-mib-table (table-oid &rest keys)
  `(def-list-based-mib-table ,table-oid (,(gensym)) ,@keys))

(defmacro def-list-based-mib-table (table-oid (arg) &rest keys)
  (check-type table-oid object-id)
  (let ((list-form (cdr (assoc :list keys)))
	(entry-oid (cdr (assoc :entry keys)))
	(index-function (cadr (assoc :index keys)))
	(test-if (cdr (assoc :test-if keys)))
	(test-if-not (cdr (assoc :test-if-not keys)))
	(key-fn (or (cdr (assoc :key keys)) #'identity)))
    (declare (type (or null (function (t) t))
		   test-if
		   test-if-not
		   key-fn))
    (let ((test-fn (cond (test-if
			  #'(lambda (tbl entry) (declare (ignore tbl))
			      (funcall test-if (funcall key-fn entry))))
			 (test-if-not
			  #'(lambda (tbl entry) (declare (ignore tbl))
			      (not (funcall test-if-not (funcall key-fn entry)))))
			 (t #'(lambda (tbl entry) (declare (ignore tbl entry))
				t)))))
      (declare (type (function (t t) t) test-fn))
      (if (and (listp list-form) (endp (rest list-form)))
	  (setq list-form (car list-form))
	(error "Syntax error: (:list <form>) expected"))
      (if (and (listp entry-oid) (endp (rest entry-oid))
	       (typep (setq entry-oid (first entry-oid)) 'object-id))
	  (setq entry-oid (object-id-subids entry-oid))
	(error "Syntax error: (:entry <oid>) expected"))
      (let ((min-subid nil) (max-subid nil) (clauses '())
	    (entry-var (gensym "ENTRY"))
	    (subindex-var (gensym "SUBINDEX"))
	    (restoid (gensym "RESTOID")))
	(dolist (slot (cdr (assoc :slots keys)))
	  (unless (typep (first slot) 'object-id)
	    (error "(<oid> (args...) forms...) expected"))
	  (let ((slot-oid (object-id-subids (first slot))))
	    (unless (list-prefix-p entry-oid slot-oid)
	      (error "Entry oid ~S is not a prefix of slot oid ~S"
		     (make-object-id entry-oid)
		     (first slot)))
	    (let ((slot-rest (nthcdr (length entry-oid) slot-oid)))
	      (unless (= (length slot-rest) 1)
		(error "Slot oid ~S is ~D levels below entry oid ~S"
		       (first slot)
		       (length slot-rest)
		       (make-object-id entry-oid)))
	      (let ((subindex (first slot-rest)))
		(when (or (not min-subid) (> min-subid subindex))
		  (setq min-subid subindex))
		(when (or (not max-subid) (< max-subid subindex))
		  (setq max-subid subindex))
		(push
		 (labels ((pairlis* (l1 l2)
			    (pairlis*-aux l1 l2 '()))
			  (pairlis*-aux (l1 l2 result)
			    (if (endp l1) 
				result
			      (pairlis*-aux (rest l1) (rest l2)
					    (cons (cons (first l1) (first l2))
						  result)))))
		   (cons subindex
			 (sublis (pairlis*
				  (cadr slot)
				  (list entry-var subindex-var restoid))
				 (cddr slot))))
		 clauses)))))
	(let ((get-component-fns '()))
	  (dotimes (subid (1+ max-subid))
	    (let ((clause (assoc subid clauses :test #'=)))
	      (let ((form (cdr clause)))
		(push
		 (and clause
		      `#'(lambda (,entry-var ,restoid)
			   (declare (ignorable ,restoid))
			   ,@form))
		 get-component-fns))))
	  (setf get-component-fns (nreverse get-component-fns))
	  `(add-node-with-values-to-id-tree
	    ',entry-oid
	    (list (make-instance ',(if index-function 
				       'complex-listy-mib-table
				     'simple-listy-mib-table)
		    :test-fn ,test-fn
		    :name ,(princ-to-string table-oid)
		    :entry-size ,max-subid
		    ,@(and index-function
			   `(:entry-index-function ,index-function))
		    :get-list-fn 
		    #'(lambda (table ,arg)
			(declare (ignorable table ,arg))
			,(if (cadr (assoc :sort keys))
			     `(sort (copy-seq ,list-form)
				    ,(or  (cadr (assoc :compare-function keys))
					 `#'(lambda (x y)
					      (oid-list-<
					       (table-entry-index table x)
					       (table-entry-index table y)))))
			   list-form))
		    :get-component-fns (vector ,@get-component-fns)))
	    (mib-id-tree *mib*)
	    #'(lambda (x) (typep x 'mib-variable))))))))

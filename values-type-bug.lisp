(in-package :common-lisp-user)

(declaim (ftype (function (t t) (values t t)) double-identity))

(defun double-identity (a b) (values a b))

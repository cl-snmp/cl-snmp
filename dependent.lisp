;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  dependent.lisp
;;; Description:  System-dependent definitions
;;; Author:	  Simon Leinen (simon@liasg3)
;;; Date Created: 22-Feb-94
;;; RCS $Header: /project/cl-snmp/cvsroot/cl-snmp/dependent.lisp,v 1.7 2006/03/28 20:30:22 sleinen Exp $
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :snmp)

#+(and :allegro (or :svr4 :linux) (version>= 4 2))
(pushnew :foreign-load-so *features*)

#-(or :cmu :lucid)
(pushnew :condition-multiple-inheritance *features*)

;;; DEFINE-LOAD-FORM-MAKER
;;;
;;; The purpose of this macro is to shield the rest of the code from
;;; the difference in signatures of MAKE-LOAD-FORM between
;;; implementations.  X3J13 has changed the draft ANSI standard
;;; definition of MAKE-LOAD-FORM to accept an (optional) second
;;; argument containing the environment.  If your Lisp implements this
;;; new behavior, you need the first definition, if not, use the
;;; second.
;;;
(defmacro define-load-form-maker ((binding) &body body)
  `(defmethod make-load-form (,binding &optional .environment.)
     (declare (ignore .environment.))
     ,@body))

(defparameter default-mib-pathname
    (merge-pathnames #p"objects.defs"
		     #+asdf
		     (or 
		      (and (asdf:find-system :snmp)
			   (slot-value (asdf:find-system :snmp) 'asdf::relative-pathname ))
		      *load-pathname*) 
		     #-:asdf *load-pathname*))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  mib.lisp
;;; Description:  Definition of the MIB Datatype
;;; Author:	  Simon Leinen (simon@liasun2)
;;; Date Created: 29-May-92
;;; RCS $Header: /home/leinen/cl/sysman/mib/RCS/defs.lisp,v 1.14 2002/08/11 21:54:27 leinen Exp leinen $  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :snmp)

;;; The current MIB.
(defvar *mib*)

;;; MIB
;;;
;;; This is the data type describing the Management Information Base.
;;; Its main component is the ID-TREE, which maps object identifiers
;;; (OIDs) to variables and their names.  The structure of the id-tree
;;; looks like this:
;;;
;;; (nil (1 ("iso")
;;;	    (3 ("org")
;;;	       (6 ("dod")
;;;		  (1 ("internet")
;;;		     (1 ("directory"))
;;;		     (2 ("mgmt")
;;;			(1 ("mib-2")
;;;			   (1 ("system")
;;;			      (1 ("sysDescr" #<MIB-VARIABLE sysDescr>))
;;;			      (2 ("sysObjectID" #<MIB-VARIABLE sysObjectID>))
;;;			      ...)
;;;			   (2 ("interfaces")
;;;			      (1 ("ifNumber") ...)
;;;			      ...)
;;;				   ...)))))))
;;;
;;; Note that this structure is somewhat wasteful: it would be more
;;; efficient if the subtrees preceded the values in the parent lists.
;;; Unfortunately this would make the printed representation more
;;; difficult to read.  Once this has been tested, this should be
;;; slightly redesigned.
;;;
(defstruct (mib
	    (:copier nil)
	    (:predicate nil)
	    (:print-function
	     (lambda (mib stream level)
	       (declare (ignore level))
	       (print-unreadable-object (mib stream :type t)
		 (format stream "~A [~A] ~D"
			 (or (mib-pretty-name mib)
			     (mib-name mib))
			 (mib-source mib)
			 (mib-def-count mib))))))
  (source nil)
  (name nil)
  (pretty-name nil)
  (def-count 0 :type (integer 0))
  (name-to-id-table nil :type (or null hash-table))
  (id-tree (list nil (list* 1 (list "iso") '())) :type list)
  (default-oid nil))

(define-load-form-maker ((a mib))
  (make-load-form-saving-slots a))

(defmethod describe-object ((mib mib) stream)
  (format stream "~S is a ~S." mib (type-of mib)))

(defmethod describe-object :after ((mib mib) stream)
  (format stream "~&  source: ~S" (mib-source mib))
  (format stream "~&  ~D definitions" (mib-def-count mib)))

;;; Operations on ID-TREEs

(defmacro id-tree-subtrees (id-tree)
  `(the list (cdr ,id-tree)))

(defun id-tree-nth-subtree (id-tree n)
  (cdr (assoc n (id-tree-subtrees id-tree) :test #'eql)))

(defun id-tree-next-subtrees (id-tree n)
  (declare (type oid-component n))
  (member-if
   #'(lambda (x)
       (> (the oid-component (car x)) n))
   (id-tree-subtrees id-tree)))

(defmacro id-tree-values (id-tree)
  `(the list (car ,id-tree)))

(defun id-tree-add-nth-subtree (tree n values)
  (declare (type oid-component n))
  (setf (id-tree-subtrees tree)
    (merge 'list
	   (list (list* n values '()))
	   (id-tree-subtrees tree)
	   #'< :key #'car)))

(defun add-node-with-values-to-id-tree (oid values tree &optional remove-fn)
  (let ((subtree (id-tree-nth-subtree tree (first oid))))
    (if subtree
	(if (endp (rest oid))
	    (setf (id-tree-values subtree)
	      (append 
	       values 
	       (if remove-fn
		   (remove-if remove-fn (id-tree-values subtree))
		 (id-tree-values subtree))))
	  (add-node-with-values-to-id-tree (rest oid) values subtree))
      (if (endp (rest oid))
	  (id-tree-add-nth-subtree tree (first oid) values)
	(let ((new-tree (id-tree-add-nth-subtree tree (first oid) '())))
	  (add-node-with-values-to-id-tree (rest oid) values new-tree)
	  tree))))
  tree)

(defun print-oid-with-mib (oid stream &optional (mib *mib*))
  (let ((default-oid (mib-default-oid mib))
	   (subids (object-id-subids oid)))
    (let ((decoded-oid 
	   (mib-decode-oid *mib* subids)))
      (if (and (> (length subids)
		  (length default-oid))
	       (equal (subseq subids 0 (length default-oid))
		      default-oid))
	  (format stream "~{~A~^.~}" (subseq decoded-oid (length default-oid)))
	(format stream "~{.~A~}" decoded-oid)))))

(defun mib-name-to-id (mib name)
  (if (string= name "iso")
      '(1)
    (let ((entry (gethash name (mib-name-to-id-table mib))))
      (unless entry (error 'mib-inexistent-name-error :mib mib :name name))
      entry)))

(defun mib-encode-oid (mib oid)
  (do ((oid oid (append (mib-name-to-id mib (first oid)) (rest oid))))
      ((numberp (first oid)) oid)))

(defun mib-decode-1 (tree oid names)
  (if (endp oid)
      (nreverse names)
      (let ((subtree (id-tree-nth-subtree tree (first oid)))
	    name)
	(if (and subtree (setq name (find-if #'stringp (id-tree-values subtree))))
	    (mib-decode-1
	     subtree
	     (rest oid)
	     (cons name names))
	    (revappend names oid)))))

(defun mib-decode-oid (mib oid)
  (mib-decode-1 (mib-id-tree mib) oid '()))

(define-condition mib-error (error)
  ((mib :initarg :mib :reader mib-error-mib)))

(define-condition mib-inexistent-name-error (mib-error)
  ((name :initarg :name :reader mib-inexistent-name))
  (:report (lambda (c stream)
	     (format stream "Name ~S not found in mib ~S"
		     (mib-inexistent-name c)
		     (mib-error-mib c)))))

(defun id-tree-get (tree subids)
  (let ((variable (find-if #'(lambda (x) (typep x 'mib-variable))
			   (id-tree-values tree))))
    (if variable
	(values variable subids)
      (let ((subtree (id-tree-nth-subtree tree (first subids))))
	(if subtree
	    (id-tree-get subtree (rest subids))
	  nil)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  oid-reader.lisp
;;; Description:  Read macro for OIDs
;;; Author:	  Simon Leinen (simon@liasun2)
;;; Date Created: 31-May-92
;;; RCS $Header: /home/leinen/cl/sysman/mib/RCS/oid-reader.lisp,v 1.3 2000/04/08 20:15:06 simon Exp leinen $  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; A readmacro is defined that faciliates entering object IDs
;;; relative to the current MIB.
;;;
;;; SNMP(43): [sysContact.0]
;;; #<OBJECT-ID system.sysContact.0>
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(in-package :snmp)

(defun oid-reader (stream char)
  (let ((close-char (ecase char
		      ((#\[) #\])
		      ((#\{) #\})
		      ((#\() #\))))
	(token-separator #\.)
	(tokens '())
	(chars '()))
    (labels
	((finish-token ()
	   (assert (not (endp chars)))
	   (push (if (every #'digit-char-p chars)
		     (reduce #'(lambda (b a)
				 (+ (* 10 a) (digit-char-p b 10)))
			     chars
			     :from-end t
			     :initial-value 0)
		   (coerce (nreverse chars) 'string))
		 tokens)
	   (setq chars '())))
      (loop
	(let ((next-char (peek-char nil stream)))
	  (cond ((char= next-char close-char)
		 (read-char stream)
		 (finish-token)
		 (return
		   (make-object-id
		    (mib-encode-oid *mib* (nreverse tokens)))))
		((char= next-char token-separator)
		 (read-char stream)
		 (finish-token))
		(t (push (read-char stream) chars))))))))

(defvar *snmp-readtable* (copy-readtable nil))
(set-macro-character #\[ #'oid-reader t *snmp-readtable*)

#+allegro
(push (cons :snmp *snmp-readtable*) excl::*named-readtables*)

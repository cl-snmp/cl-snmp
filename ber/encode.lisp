;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File Name:	  ber-encode.lisp
;;; Description:  BER Encoding of ASN.1 Data
;;; Author:	  Simon Leinen (simon@liasun1)
;;; Date Created: 28-May-92
;;; RCS $Header: /project/cl-snmp/cvsroot/cl-snmp/ber/encode.lisp,v 1.10 2002/08/11 21:54:25 leinen Exp $  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :asn.1)

(defconstant default-ber-buffer-size 2048)

(defstruct (ber-output-buffer
	    (:predicate nil)
	    (:copier nil)
	    (:constructor make-ber-output-buffer ())
	    (:print-function (lambda (buffer stream level)
			       (declare (ignore level))
			       (print-unreadable-object (buffer stream
							 :type t
							 :identity t)))))
  (byte-array (make-array default-ber-buffer-size
			  :element-type '(unsigned-byte 8))
	      :type (simple-array (unsigned-byte 8) (*)))
  (position 0 :type ber-buffer-pointer))

(declaim 
 (ftype (function () ber-output-buffer) get-ber-output-buffer)
 (ftype (function (ber-output-buffer) t) free-ber-output-buffer)
 (ftype (function (ber-output-buffer) (simple-array (unsigned-byte 8) (*)))
	ber-buffer->pdu)
 (ftype (function (ber-output-buffer (unsigned-byte 8)) t)
	ber-write-byte)
 (ftype (function (ber-output-buffer (simple-array (unsigned-byte 8) (*))) t)
	ber-write-ber-buffer)
 (ftype (function (ber-output-buffer ber-length) t)
	ber-write-length)
 (ftype (function (ber-output-buffer ber-tag ber-length) t)
	ber-write-header)
 (ftype (function (ber-output-buffer simple-string &optional ber-tag) t)
	ber-write-string)
 (ftype (function (ber-output-buffer (simple-array (unsigned-byte 8) (*))
				     &optional ber-tag) t)
	ber-write-octet-string)
 (ftype (function (ber-output-buffer integer &optional ber-tag) t)
	ber-write-int)
 (ftype (function (ber-output-buffer &optional ber-tag) t)
	ber-write-null)
 (ftype (function (ber-output-buffer object-id &optional ber-tag) t)
	ber-write-object-id)
 (ftype (function (ber-output-buffer sequence &optional ber-tag) t)
	ber-write-sequence)
 (ftype (function (ber-output-buffer typed-asn-tuple) t)
	ber-write-typed-asn-tuple)
 (ftype (function (ber-output-buffer t) t)
	ber-write)
 (ftype (function (t) (simple-array (unsigned-byte 8) (*)))
	ber-encode))

(defvar *ber-buffer-resource* '())
(defun get-ber-output-buffer ()
  (or (pop *ber-buffer-resource*)
      (make-ber-output-buffer)))
(defun free-ber-output-buffer (buf)
  (setf (ber-output-buffer-position buf) 0)
  (push buf *ber-buffer-resource*))

(defmacro with-output-to-ber-buffer ((ber-buffer) &body body)
  `(let ((,ber-buffer (get-ber-output-buffer)))
     (unwind-protect
	 (progn ,@body (ber-buffer->pdu ,ber-buffer))
       (free-ber-output-buffer ,ber-buffer))))

(defun ber-buffer->pdu (buffer)
  (subseq (ber-output-buffer-byte-array buffer)
	  0 (ber-output-buffer-position buffer)))

(defun ber-write-byte (buffer byte)
  (declare (type (unsigned-byte 8) byte))
  (prog1
      (setf (aref (ber-output-buffer-byte-array buffer)
		  (ber-output-buffer-position buffer))
	byte)
    (incf (ber-output-buffer-position buffer))))

(defun ber-write-ber-buffer (dest buffer)
  (dotimes (index (length buffer) buffer)
    (ber-write-byte dest (aref buffer index))))

(defun ber-write-length (buffer length)
  (declare (type (integer 0) length))
  (cond ((< length 128)
	 (ber-write-byte buffer length))
	((< length 256)
	 (ber-write-byte buffer (logior ber-long-len 1))
	 (ber-write-byte buffer length))
	((< length 65536)
	 (ber-write-byte buffer (logior ber-long-len 2))
	 (ber-write-byte buffer (ldb (byte 8 8) length))
	 (ber-write-byte buffer (ldb (byte 8 0) length)))
	(t (error 'ber-unencodable-length length))))

(defun ber-write-header (buffer type length)
  (ber-write-byte buffer type)
  (ber-write-length buffer length))

(defun ber-write-string (buffer string &optional (type ber-octet-string-tag))
  (ber-write-header buffer type (length string))
  (dotimes (index (length string))
    (ber-write-byte buffer (char-code (char string index)))))

(defun ber-write-octet-string (buffer string
			       &optional (type ber-octet-string-tag))
  (ber-write-header buffer type (length string))
  (dotimes (index (length string))
    (ber-write-byte buffer (aref string index))))

(defun ber-write-int (buffer int &optional (type ber-int-tag))
  (let ((int-length-in-bytes (ceiling (1+ (integer-length int)) 8)))
    (ber-write-header buffer type int-length-in-bytes)
    (do ((i (1- int-length-in-bytes) (1- i)))
	((< i 0))
      (declare (type integer i))
      (ber-write-byte buffer (ldb (byte 8 (* i 8)) int)))))

(defun ber-write-null (buffer &optional (type ber-null-tag))
  (ber-write-header buffer type 0))

(defun ber-write-object-id (buffer object-id &optional (type ber-object-id-tag))
  (let ((subids (object-id-subids object-id)))
    (when (> (length subids) 2)
      (setq subids (cons (the oid-component
			      (+ (* 40 (the oid-component (first subids)))
				 (the oid-component (second subids))))
			 (cddr subids))))
    (let ((subid-lengths (mapcar #'(lambda (subid)
				     (declare (type oid-component subid))
				     (max 1 (ceiling (integer-length subid) 7)))
				 subids)))
      (ber-write-header buffer type (reduce #'+ subid-lengths))
      (do ((subids subids (rest subids))
	   (subid-lengths subid-lengths (rest subid-lengths)))
	  ((endp subids))
	(let ((subid (first subids))
	      (subid-length (first subid-lengths)))
	  (do ((count (* (1- subid-length) 7) (- count 7)))
	      ((< count 0))
	    (let ((byte (ldb (byte 7 count) subid)))
	      (ber-write-byte buffer
			      (if (< count 7)
				  byte
				  (logior byte ber-bit8))))))))))

(defun ber-write-sequence (buffer sequence &optional (type ber-sequence-tag))
  (let ((content
	 (with-output-to-ber-buffer (s)
	   (dolist (item sequence)
	     (ber-write s item)))))
    (ber-write-header buffer (logior type ber-constructor) (length content))
    (ber-write-ber-buffer buffer content)))

(defun ber-write-typed-asn-tuple (buffer item)
  (ber-write-sequence
   buffer
   (typed-asn-tuple-elements item)
   (typed-asn-tuple-type item)))

(defun ber-write (buffer item)
  (typecase item
    (null (ber-write-null buffer))
    (list (ber-write-sequence buffer item))
    (integer (ber-write-int buffer item))
    (string (ber-write-string buffer item))
    ((simple-array (unsigned-byte 8) (*))
     (ber-write-octet-string buffer item))
    (object-id (ber-write-object-id buffer item))
    (typed-asn-tuple (ber-write-typed-asn-tuple buffer item))
    (t (funcall (the function *application-specific-ber-encoder*)
		buffer item))))

(defun ber-encode (item)
  (with-output-to-ber-buffer (s)
    (ber-write s item)))
